package dbmanager

import (
	"messaging/models"

	_ "github.com/go-sql-driver/mysql" // MySQL driver
)

//seed database
func RunMigrations() {
	db := Connect()
	//Droping tables
	db.DropTable("users")
	db.DropTable("rooms")
	db.DropTable("conversations")
	db.DropTable("notifications")
	//db.DropTable("userooms")
	usr := models.User{}
	room := models.Room{}
	conversation := models.Conversation{}
	notification := models.Notification{}

	//Creating tables
	db.CreateTable(&usr)
	db.CreateTable(&room)
	db.CreateTable(&conversation)
	db.CreateTable(&notification)
	supportUser := models.User{Userid: "11E794856A9FD5F48B92087402FFFEE4", Username: "support", Password: "support", Email: "support@yappapp.com", Connected: true, Isdeleted: false, AuthId: "11E794856A9FD5F48B92087402FFFEE4"}
	db.Create(&supportUser)

	//user1 := models.User{Userid: "11E794856AA634448B92087402FFFEE4 ", Username: "user1", Password: "user1", Email: "user1@yappapp.com", Connected: true, Isdeleted: false, AuthId: "11E794856A9FD5F48B92087402FFFEE4"}
	//db.Create(&user1)

	//	user2 := models.User{Userid: "11E7A1C35DA6BD199708063C6BE6EACA", Username: "user2", Password: "user2", Email: "user2@yappapp.com", Connected: true, Isdeleted: false, AuthId: "11E794856A9FD5F48B92087402FFFEE4"}
	//db.Create(&user2)
	/*
		//Sample data
		//Test
		uTest := models.User{Userid: "11E77BFA575041F7A10A124B743B4A6C", Username: "Test", Password: "Test", Email: "test@test.com", Connected: true, Isdeleted: false}
		db.Create(&uTest)
		//Test1
		uTest1 := models.User{Userid: "5994273b3d58c405a36ff3d8", Username: "Spiderman", Password: "spiderman", Email: "test@test.com", Connected: true, Isdeleted: false}
		db.Create(&uTest1)

		//Test2
		uTest2 := models.User{Userid: "598462210f2a2009b6650dbe", Username: "Wolverine", Password: "wolverine", Email: "test@test.com", Connected: true, Isdeleted: false}
		db.Create(&uTest2)

		u1 := models.User{Userid: "1", Username: "malkeet", Password: "malkeet", Email: "malkeet@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u1)

		u2 := models.User{Userid: "2", Username: "Amrit", Password: "Amrit", Email: "amrit@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u2)

		u3 := models.User{Userid: "3", Username: "Rohit", Password: "Rohit", Email: "rohit@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u3)

		u4 := models.User{Userid: "4", Username: "Sandeep", Password: "Sandeep", Email: "sandeep@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u4)

		u5 := models.User{Userid: "5", Username: "Manjeet", Password: "Manjeet", Email: "manjeet@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u5)

		u6 := models.User{Userid: "6", Username: "Vineet", Password: "Vineet", Email: "Vineet@yahoo.com", Connected: true, Isdeleted: false}
		db.Create(&u6)
		//Rooms
		//TestRooms
		rTest1 := models.Room{Roomid: "TestRoom", Userone: uTest.Userid, Usertwo: u2.Userid}
		db.Create(&rTest1)
		rTest2 := models.Room{Roomid: "TestRoom", Userone: u2.Userid, Usertwo: uTest.Userid}
		db.Create(&rTest2)
		rTestA := models.Room{Roomid: "59a660ea32241e064b99ffdf", Userone: uTest1.Userid, Usertwo: uTest2.Userid}
		db.Create(&rTestA)
		rTestB := models.Room{Roomid: "59a660ea32241e064b99ffdf", Userone: uTest2.Userid, Usertwo: uTest1.Userid}
		db.Create(&rTestB)

		//u1=>u2
		r1 := models.Room{Roomid: "1", Userone: u1.Userid, Usertwo: u2.Userid}
		db.Create(&r1)
		r2 := models.Room{Roomid: "1", Userone: u2.Userid, Usertwo: u1.Userid}
		db.Create(&r2)
		//u1=>u3
		r3 := models.Room{Roomid: "2", Userone: u1.Userid, Usertwo: u3.Userid}
		db.Create(&r3)
		r4 := models.Room{Roomid: "2", Userone: u3.Userid, Usertwo: u1.Userid}
		db.Create(&r4)
		//u1=>u4
		r5 := models.Room{Roomid: "3", Userone: u1.Userid, Usertwo: u4.Userid}
		db.Create(&r5)
		r6 := models.Room{Roomid: "3", Userone: u4.Userid, Usertwo: u1.Userid}
		db.Create(&r6)
		//u1=>u5
		r7 := models.Room{Roomid: "4", Userone: u1.Userid, Usertwo: u5.Userid}
		db.Create(&r7)
		r8 := models.Room{Roomid: "4", Userone: u5.Userid, Usertwo: u1.Userid}
		db.Create(&r8)
		//u1=>u6
		r9 := models.Room{Roomid: "5", Userone: u1.Userid, Usertwo: u6.Userid}
		db.Create(&r9)
		r10 := models.Room{Roomid: "5", Userone: u6.Userid, Usertwo: u1.Userid}
		db.Create(&r10)

		//u2=>u3
		r11 := models.Room{Roomid: "6", Userone: u2.Userid, Usertwo: u3.Userid}
		db.Create(&r11)
		r12 := models.Room{Roomid: "6", Userone: u3.Userid, Usertwo: u2.Userid}
		db.Create(&r12)
		//u2=>u4
		r13 := models.Room{Roomid: "7", Userone: u2.Userid, Usertwo: u4.Userid}
		db.Create(&r13)
		r14 := models.Room{Roomid: "7", Userone: u4.Userid, Usertwo: u2.Userid}
		db.Create(&r14)
		//u2=>u5
		r15 := models.Room{Roomid: "8", Userone: u2.Userid, Usertwo: u5.Userid}
		db.Create(&r15)
		r16 := models.Room{Roomid: "8", Userone: u5.Userid, Usertwo: u2.Userid}
		db.Create(&r16)
		//u2=>u6
		r17 := models.Room{Roomid: "9", Userone: u2.Userid, Usertwo: u6.Userid}
		db.Create(&r17)
		r18 := models.Room{Roomid: "9", Userone: u6.Userid, Usertwo: u2.Userid}
		db.Create(&r18)

		//u3=>u4
		r19 := models.Room{Roomid: "10", Userone: u3.Userid, Usertwo: u4.Userid}
		db.Create(&r19)
		r20 := models.Room{Roomid: "10", Userone: u4.Userid, Usertwo: u3.Userid}
		db.Create(&r20)
		//u3=>u5
		r21 := models.Room{Roomid: "11", Userone: u3.Userid, Usertwo: u5.Userid}
		db.Create(&r21)
		r22 := models.Room{Roomid: "11", Userone: u5.Userid, Usertwo: u3.Userid}
		db.Create(&r22)
		//u3=>u6
		r23 := models.Room{Roomid: "12", Userone: u3.Userid, Usertwo: u6.Userid}
		db.Create(&r23)
		r24 := models.Room{Roomid: "12", Userone: u6.Userid, Usertwo: u3.Userid}
		db.Create(&r24)

		//u4=>u5
		r25 := models.Room{Roomid: "14", Userone: u4.Userid, Usertwo: u5.Userid}
		db.Create(&r25)
		r26 := models.Room{Roomid: "14", Userone: u5.Userid, Usertwo: u4.Userid}
		db.Create(&r26)
		//u4=>u6
		r27 := models.Room{Roomid: "15", Userone: u4.Userid, Usertwo: u6.Userid}
		db.Create(&r27)
		r28 := models.Room{Roomid: "15", Userone: u6.Userid, Usertwo: u4.Userid}
		db.Create(&r28)

		//u5=>u6
		r29 := models.Room{Roomid: "16", Userone: u5.Userid, Usertwo: u6.Userid}
		db.Create(&r29)
		r30 := models.Room{Roomid: "16", Userone: u6.Userid, Usertwo: u5.Userid}
		db.Create(&r30)

		/*
			r3 := models.Room{Roomid: 3, Userone: u1.Userid, Usertwo: u4.Userid}
			db.Create(&r3)
			r4 := models.Room{Roomid: 4, Userone: u1.Userid, Usertwo: u3.Userid}
			db.Create(&r4)
			r5 := models.Room{Roomid: 5, Userone: u6.Userid, Usertwo: u1.Userid}
			db.Create(&r5)
			r6 := models.Room{Roomid: 6, Userone: u6.Userid, Usertwo: u2.Userid}
			db.Create(&r6)
			r7 := models.Room{Roomid: 7, Userone: u6.Userid, Usertwo: u3.Userid}
			db.Create(&r7)*/

	/*
		r1 := models.Room{Roomid: 1, RoomTitle: "Red", Userid: u1.Userid}
		db.Create(&r1)

		r2 := models.Room{Roomid: 2, RoomTitle: "Yellow", Userid: u2.Userid}
		db.Create(&r2)

		ur1 := models.UserRoom{FriendId: u2.ID, RoomId: r1.ID}
		db.Create(&ur1)
		ur2 := models.UserRoom{FriendId: u1.ID, RoomId: r2.ID}
		db.Create(&ur2)
	*/
	defer db.Close()
}
