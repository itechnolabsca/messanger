package models

import (
	"errors"
	"fmt"
	"messaging/utils"
	"strconv"
)

type Conversation struct {
	ID            int64
	Roomid        string
	From          string //From User
	To            string //To Userid
	Message       string
	CreatedAt     int64 //need to get in unix format
	DeliveredAt   int64 //need to get Deliver date
	ReadAt        int64 //Read datetime
	UpdatedAt     int64 //updated time
	Delivered     bool
	Read          bool
	LocalId       string //sender's localid
	OperationType string //senders
	Type          string //sender
	Thumb         string //sender
	MediaThumb    string //sender
	Caption       string //sender
}

func (c *Conversation) TableName() string {
	return "conversations"
}

//Save conversation
func CreateConverstion(from, to, msg, roomid, localID, operationType, msgType, thumb, mediaThumb, caption string) (isSaved bool, conversation Conversation) {
	db := getDBConnection()
	defer db.Close()
	result := Conversation{Roomid: roomid, From: from, To: to, Message: msg, CreatedAt: utils.MakeTimestampMilli(), Read: false, Delivered: false, DeliveredAt: 0, LocalId: localID, OperationType: operationType, Type: msgType, Thumb: thumb, MediaThumb: mediaThumb, Caption: caption}
	db.Create(&result)
	//db.NewRecord(result)
	fmt.Printf("%v", result)
	return true, result

}

/**********************************UPDATE CONVERSATION ********************************************************************************/
func UpdateConversation(senderID string, roomID string, msgID string, delivered bool, read bool) (conversationArr []Conversation, err error) {
	var conversation Conversation
	db := getDBConnection()
	defer db.Close()
	convertedMsgID, _ := strconv.ParseInt(msgID, 10, 64)
	//conversation = Conversation{To: senderID, Roomid: roomID, ID: convertedMsgID} //TO who is receiver of this message
	db.Find(&conversation, &Conversation{ID: convertedMsgID})
	//conversation.Delivered = delivered
	if delivered {
		conversation.DeliveredAt = utils.MakeTimestampMilli() //can be change with provided time
		conversation.Delivered = true
	} /* else {
		conversation.DeliveredAt = 0
		conversation.Delivered = false
	} */
	//conversation.Read = read
	if read {
		conversation.ReadAt = utils.MakeTimestampMilli() //can be change with provided time
		conversation.Read = true
	} /*else {
		conversation.ReadAt = 0
		conversation.Read = false
	}*/
	conversation.UpdatedAt = utils.MakeTimestampMilli() //need to change the update time as soon as any update notifiction comes in

	db.Save(&conversation)
	var convList []Conversation
	/*********CONVESION STRING TO UINT****/
	u64, err := strconv.ParseInt(msgID, 10, 64)
	if err != nil {
		fmt.Println(err)
	}
	wd := u64 //uint64(u64)
	allConvs := []Conversation{}
	//db.Find(&allUsers)
	db.Where(&Conversation{ID: wd}).Find(&allConvs)
	//db.Find(&allConvs) //&convList, &Conversation{ID: wd}) //Room{ID: wd})
	convList = allConvs
	if len(convList) > 0 {
		return convList, nil
	}
	return convList, errors.New("Unable to find rooms for this user ")
}

/**********************************DELETE CONVERSATION ********************************************************************************/
func DeleteConversation(msgID string) (mID Conversation) {
	//var UsrIds string
	db := getDBConnection()
	defer db.Close()
	//for _, msgID := range msgIDs {
	convertedMsgID, _ := strconv.ParseInt(msgID, 10, 64) //strconv.FormatInt(msgID, 10, 64)
	conversation := Conversation{ID: convertedMsgID}     //TO who is receiver of this message
	db.Find(&conversation)
	conversation.OperationType = "deleted"
	conversation.UpdatedAt = utils.MakeTimestampMilli()
	db.Save(&conversation)
	//UsrIds = conversation.To //append(UsrIds, conversation.To)
	//}
	return conversation //UsrIds
}

/***************************************************END DELETE CONVERSATION***************************************/
/********************************************************GETALL CONVERSATIONS *************************************************************/

func GetAllMessagesByUserID(senderID string, dateTimeLimit int64) (conversationArr []Conversation, err error) {
	db := getDBConnection()
	defer db.Close()
	convList := []Conversation{}
	//parsedDateTimeLimit, _ := strconv.ParseInt(dateTimeLimit, 10, 64)
	parsedDateTimeLimit := dateTimeLimit
	//db.Where(&Conversation{From: senderID, CreatedAt: parsedDateTimeLimit}).Find(&convList)
	conv := []Conversation{}
	//db.Where("From = ?  AND CreatedAt <= ?", senderID, parsedDateTimeLimit).Find(&conv)
	//db.Raw("SELECT * FROM conversations WHERE conversations.from = ? OR conversations.to = ? AND conversations.updated_at=?", senderID, senderID, parsedDateTimeLimit).Scan(&conv)
	db.Raw("SELECT * FROM conversations WHERE  conversations.from = ? AND conversations.updated_at >= ?", senderID, parsedDateTimeLimit).Scan(&conv)
	//db.Find(&conv, "From = ?  AND CreatedAt <= ?", senderID, parsedDateTimeLimit)

	convList = conv
	if len(convList) > 0 {
		return convList, nil
	}
	return convList, errors.New("Unable to find messages for this user ")

}

/**********************************************************GET MESSAGEBYID*******************************/
func GetConversationById(covID int64) Conversation {
	db := getDBConnection()
	defer db.Close()
	conversation := Conversation{ID: covID} //TO who is receiver of this message
	db.Find(&conversation)
	return conversation
}
