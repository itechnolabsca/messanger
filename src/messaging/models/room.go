package models

import (
	"errors"
	"fmt"
	"messaging/utils"

	_ "github.com/go-sql-driver/mysql"
)

type Room struct {
	ID      uint
	Roomid  string
	Userone string
	Usertwo string
}

func (r *Room) TableName() string {
	return "rooms"
}

/*********************GET ALL ROOMS*****************/
func GetAllRooms() []string {
	var roomArr []string
	db := getDBConnection()
	defer db.Close()
	allRooms := []Room{}
	db.Find(&allRooms)
	for _, room := range allRooms {
		strRoomid := fmt.Sprint(room.Roomid)
		roomArr = append(roomArr, strRoomid)
	}
	result := utils.RemoveDuplicates(roomArr)
	return result
}

/****************************************GET USERS FIEND FOR ONLINE STATUS**************/
func GetRoomsByUserIdForOnline(userID string) ([]Room, error) {
	roomArr := []Room{}
	db := getDBConnection()
	defer db.Close()
	db.Where(&Room{Usertwo: userID}).Find(&roomArr)
	if len(roomArr) > 0 {
		return roomArr, nil
	}
	return nil, errors.New("Unable to find rooms for this user ")
}

/************RETURNS ALL ROOMS OF USER**************/
func GetRoomsByUserId(userID string) ([]Room, error) {
	roomArr := []Room{}
	db := getDBConnection()
	defer db.Close()
	db.Where(&Room{Userone: userID}).Find(&roomArr)
	if len(roomArr) > 0 {
		return roomArr, nil
	}
	return nil, errors.New("Unable to find rooms for this user ")
}
func GetRoomsByUserIDandRoomID(userID string, roomID string) ([]Room, error) {

	roomArr := []Room{}
	db := getDBConnection()
	defer db.Close()
	db.Where(&Room{Userone: userID, Roomid: roomID}).Find(&roomArr)
	if len(roomArr) > 0 {
		return roomArr, nil
	}
	return nil, errors.New("Unable to find rooms for this user ")
}

/**************GET LAST ROOM ID**************/
func GetNewRoomId() string {
	db := getDBConnection()
	defer db.Close()
	lastRoom := Room{}
	db.Last(&lastRoom)
	newRoomID := lastRoom.Roomid //Need to check to return last message
	return newRoomID
}
