package models

import (
	"errors"
	"messaging/utils"
	//_ "../src/github.com/go-sql-driver/mysql"
)

/**********************************************PAYLOAD FOR CREATE USER FROM KRISHAN **********************/
type NewUser struct {
	DeviceDetails struct {
		AuthID          string `json:"authId"`
		AppVersion      string `json:"appVersion"`
		ApplicationType string `json:"applicationType"`
		DeviceID        string `json:"deviceID"`
		DeviceToken     string `json:"deviceToken"`
		DeviceType      string `json:"deviceType"`
	} `json:"deviceDetails"`
	Reference string `json:"reference"`
}

/**************************************************END OF PAYLOAD *****************************************/

type User struct {
	ID          uint   `json:"id"`
	Userid      string `json:"userid"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Email       string `json:"email"`
	Connected   bool   `json:"connected"`
	Isdeleted   bool   `json:"isdeleted"`
	DeviceToken string `json:"deviceToken"`
	DeviceType  string `json:"deviceType"`
	AuthId      string `json:"authid"`
}

func (u *User) TableName() string {
	return "users"
}

/*******************************************UPDATE DEVICE TOKEN*****************************************************/

func UpdateDeviceToken(deviceToken, authId string) bool {
	var isUpdated bool
	db := getDBConnection()
	defer db.Close()
	u := User{AuthId: authId}
	db.Find(&u)
	if u.ID > 0 {
		u.DeviceToken = deviceToken
		db.Save(&u)
		isUpdated = true
	}
	return isUpdated
}

/******************************************* END UPDATE DEVICE TOKEN *****************************************************/
/********************************CREATE USER*************************************************************/

func CreateUser(usr User) (isCreated bool) {
	db := getDBConnection()
	defer db.Close()
	var user User
	db.Find(&user, &User{Userid: usr.Userid})
	if user.ID > 0 {
		return true
	}
	db.Create(&usr)
	if usr.ID > 0 {
		return true
	}
	return false
}

/*****************************END CREATEUSER **************************************************************/
/****************FIND FRIENDS BY USERName**************/
func GetFriendsByUserID(username string) ([]Room, error) {
	db := getDBConnection()
	defer db.Close()
	u := User{Username: username}
	db.Find(&u)
	if u.ID == 0 {
		return nil, errors.New("Unable to find user with username: " + username)
	}
	return GetRoomsByUserId(u.Userid)

}

/********************GET USERS FRIENDS BASED ON USERID*************/
func GetFriends(userID string, roomID string) ([]Room, error) {
	db := getDBConnection()
	defer db.Close()
	u := User{Userid: userID}
	db.Find(&u)
	if u.ID == 0 {
		return nil, errors.New("Unable to find user with userid: " + string(u.ID))
	}
	return GetRoomsByUserIDandRoomID(userID, roomID)

}

/****************************************************************ACCEPTS ARRAY OF AUTH IDS AND CREATE ROOMS **************************/
func CreateUserRooms(friends []string) (string, error) {
	var Rooms []string //need to notify to calling backend about created rooms
	db := getDBConnection()
	defer db.Close()
	roomID, err := utils.GenerateRoomId()
	if err != nil {
		panic(err)
	}
	//build rooms object & Below logic need to move to Util package
	for i := 0; i < len(friends); i++ {
		for j := 0; j < len(friends); j++ {
			if friends[i] != friends[j] {
				var room Room
				var user User
				room.Userone = friends[i]
				room.Usertwo = friends[j]
				room.Roomid = roomID
				Rooms = append(Rooms, roomID)
				//find user in user table if no then create it
				db.Find(&user, &User{Userid: friends[i]})
				if user.ID == 0 {
					var usr User
					usr.Userid = friends[i]
					usr.AuthId = friends[i]
					db.Create(&usr)
				}
				//check first if room already exists or not
				db.Find(&room, &Room{Userone: friends[i], Usertwo: friends[j]})
				if room.ID == 0 {
					//create room
					db.Create(&room)
				}
				//need to do it on goroutine
				createRoomWithSupportUser(friends[i])
			}
		}
	}
	return Rooms[0], nil

}

/*************************************************************CREATE SUPPORT USER CONNECTION**********************************/
func createRoomWithSupportUser(friend string) {
	//find if support user connection already exists
	db := getDBConnection()
	defer db.Close()
	var user User
	var room Room
	db.Find(&user, &User{Username: "support"})
	supportUserID := user.Userid
	//Find if connection with support user already exists
	db.Find(&room, &Room{Userone: supportUserID})
	if room.ID == 0 { //if not exists
		room.Userone = friend
		room.Usertwo = supportUserID
		room.Roomid, _ = utils.GenerateRoomId()
		db.Create(&room)
	}

}
