package models

import "github.com/jinzhu/gorm"

func getDBConnection() *gorm.DB {
	db, err := gorm.Open("mysql", "root:1234@tcp(127.0.0.1:3306)/chatdb")
	//defer db.Close()
	if err != nil {
		panic(err)
	}
	return db
}

/*
import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"

func getDBConnection() *sql.DB {
	db, err := sql.Open("mysql",
		"root:1234@tcp(127.0.0.1:3306)/chatdb")
	if err != nil {
		log.Fatal(err)
	}
	return db

}
*/
