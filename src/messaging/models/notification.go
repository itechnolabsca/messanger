package models

type Notification struct {
	ID          uint
	From        string
	To          string
	Message     string //needt implement big size
	DeviceType  string
	DeviceToken string
}

func (r *Notification) TableName() string {
	return "notifications"
}

/***********************************************SAVE UNDELIVERED MESSAGE DATA ON WEBSOCKETS TO DELIVER ON PUSH NOTIFICATIONS*******/
func (r *Notification) SaveNotificationData(sender, receiver, content string) {
	var user User
	db := getDBConnection()
	defer db.Close()
	db.Find(&user, &User{Userid: receiver})
	if user.ID > 0 {
		deviceType := user.DeviceType
		deviceToken := user.DeviceToken
		notification := Notification{From: sender, To: receiver, Message: content, DeviceToken: deviceToken, DeviceType: deviceType}
		db.Create(&notification)
	}

}
