package viewmodels

type UserVM struct {
	Username string
	UserId   string
	Friends  []FriendVM
}
type FriendObj struct {
	_FriendVM []FriendVM
}
type FriendVM struct {
	FriendId int
	RoomId   int
	Username string
}
