package viewmodels

type UserDeviceData struct {
	UserDevice UserDevice `json:"userDevice"`
}
type UserDevice struct {
	AccountStateID          string `json:"accountStateId"`
	ApplicationType         string `json:"applicationType"`
	CreatedOn               int    `json:"createdOn"`
	DeviceType              string `json:"deviceType"`
	ID                      string `json:"id"`
	IntID                   int    `json:"intId"`
	IsBlocked               int    `json:"isBlocked"`
	IsDeleted               int    `json:"isDeleted"`
	IsVerifiedByAdmin       int    `json:"isVerifiedByAdmin"`
	NodeJsOpIdentifier      string `json:"nodeJsOpIdentifier"`
	RegisterationSuccessful int    `json:"registerationSuccessful"`
	Scopes                  string `json:"scopes"`
	TotalSuccessfulLogins   int    `json:"totalSuccessfulLogins"`
	UpdatedOn               int    `json:"updatedOn"`
	UserID                  string `json:"userId"`
}

type RedisPayload struct {
	Token  string `json:"string"`
	UserId string `json:"string"`
	//SocketId string `json:"string"`
}
