package controllers

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"messaging/models"
	"messaging/utils"
	"messaging/viewmodels"
	"net/http"
)

type UserController struct {
}

/********************************************* UPDATE USER *************************************************/
func (this *UserController) UpdateDeviceTokenUser(w http.ResponseWriter, r *http.Request) {
	var usr models.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&usr); err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()
	if isCreated := models.UpdateDeviceToken(usr.DeviceToken, usr.AuthId); isCreated == true {
		utils.RespondWithJSON(w, http.StatusOK, "user updated")
		return
	}
	utils.RespondWithError(w, http.StatusCreated, "User not created")

}

/*********************************************END USER*****************************************************/

/*******************************************CREATE NEW USER**************************************/
func (this *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {
	var NewUser models.NewUser
	var usr models.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&NewUser); err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()
	usr.Userid = NewUser.DeviceDetails.AuthID
	usr.DeviceType = NewUser.DeviceDetails.DeviceType
	usr.DeviceToken = NewUser.DeviceDetails.DeviceToken
	if isCreated := models.CreateUser(usr); isCreated == true {
		utils.RespondWithJSON(w, http.StatusOK, "user Created")
		return
	}

	utils.RespondWithError(w, http.StatusCreated, "User not created")

}

/***************RETURNS FIREND USERS WITH ASSOCIATED ROOM ID*****************/
func (this *UserController) GetUser(w http.ResponseWriter, r *http.Request) {

	var usrVM viewmodels.UserVM
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &usrVM); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	result, err := models.GetFriendsByUserID(usrVM.Username)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}

	//utils.RespondWithJSON(w, http.StatusOK, result)

}
