package controllers

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"messaging/models"
	"messaging/utils"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type RoomController struct {
}

/**************************************************************CREATE ROOM BASED UPON AUTH TOKENS *******************/
/********************************** CREATE INDIVIDUAL MUTUAL ROOMS IN ARRAY AFTER HAVING ARRAY OF AuthID SUPPLIED ****************/
func (this *RoomController) CreateRoomsForUsers(w http.ResponseWriter, r *http.Request) {
	//Parse Auth Id Array
	decodeJson := json.NewDecoder(r.Body)
	var authIds []string
	err := decodeJson.Decode(&authIds)
	if err != nil {
		panic(err)
	}
	if rooms, err := models.CreateUserRooms(authIds); len(rooms) > 0 && err == nil { //.CreateUser(usr); isCreated == true {
		utils.RespondWithJSON(w, http.StatusOK, rooms)
		return
	}

	utils.RespondWithError(w, http.StatusNotFound, "Rooms not created")

}

/***************CREATE USER ROOMS*****************/
func (this *RoomController) CreateRoom(w http.ResponseWriter, r *http.Request) {
	var usrVM []string
	vars := mux.Vars(r)
	userid := vars["userid"]
	id, err := strconv.Atoi(userid)
	fmt.Printf("%d UserId=>", id)
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &usrVM); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}
	//create rooms for user id's and linked with provided userid
	result, err := models.CreateUserRooms(usrVM)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}
}

/***************UPDATE USER ROOMS*****************/
func (this *RoomController) UpdateRoom(w http.ResponseWriter, r *http.Request) {

}
