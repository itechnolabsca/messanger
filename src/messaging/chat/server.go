package chat

import (
	"net/http"
)

// Run starts a new chat server with 4 chat rooms, listening on port 9090
func Run() {

	// WebSocket routes.

	r := NewRoom("")
	http.Handle("/chat", r)
	go r.Run()
	/*
		roomList := models.GetAllRooms()
		if len(roomList) > 0 {
			for _, name := range roomList {
				r := NewRoom(name)
				http.Handle("/chat/"+name, r)

				go r.Run()
			}
		}*/
	//http.HandleFunc("/ws", wsPage)

}

/*
func wsPage(res http.ResponseWriter, req *http.Request) {
	conn, error := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(res, req, nil)
	if error != nil {
		http.NotFound(res, req)
		return
	}
	client := &Client{id: uuid.NewV4().String(), socket: conn, send: make(chan []byte)}

	manager.register <- client

	go client.read()
	go client.write()
}*/
