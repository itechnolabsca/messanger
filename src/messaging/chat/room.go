package chat

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"messaging/utils"
	"net/http"

	"github.com/gorilla/websocket"
)

var (
	dbPool      *utils.DBpool
	activeUsers = make(map[string]string)
)

const (
	socketBufferSize  = 1024
	messageBufferSize = 512
)

// Room represents a single chat room
type Room struct {
	broadcast chan []byte
	forward   chan []byte
	calling   chan []byte
	join      chan *Chatter
	leave     chan *Chatter
	chatters  map[*Chatter]bool
	topic     string
	typing    chan []byte

	//db        utils.Backend
}

var upgrader = &websocket.Upgrader{
	ReadBufferSize:  socketBufferSize,
	WriteBufferSize: socketBufferSize,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (r *Room) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	socket, err := upgrader.Upgrade(w, req, nil)
	sockid, _ := newUUID()
	if err != nil {
		log.Fatal("serving http failed ", err)
		return
	}

	chatter := &Chatter{
		socket:   socket,
		send:     make(chan []byte, messageBufferSize),
		room:     r,
		socketid: sockid,
	}

	r.join <- chatter
	//r.forward <- chatter
	defer func() {
		r.leave <- chatter
	}()
	go chatter.write()
	chatter.read()
}

// NewRoom creates a new chat room
func NewRoom(topic string) *Room {
	return &Room{
		typing:    make(chan []byte),
		forward:   make(chan []byte),
		calling:   make(chan []byte),
		broadcast: make(chan []byte),
		join:      make(chan *Chatter),
		leave:     make(chan *Chatter),
		chatters:  make(map[*Chatter]bool),
		topic:     topic,
	}
}

// Run initializes a chat room
func (r *Room) Run() {

	for {
		//db := //utils.GetDBInstance()
		//db := dbPool.Get()
		//db.Close()
		//defer dbPool.Close()
		dbPool = utils.GetDBPool()
		db := dbPool.Get()

		select {

		case chatter := <-r.join: //connection
			//log.Printf("new chatter in room %v with socketid %v ", r.topic, chatter.socketid)
			log.Printf("new chatter in room  with socketid %v and chatter Data %v", chatter.socketid, chatter)
			r.chatters[chatter] = true
			MessageInfo := &Message{StatusCode: 200, Type: "connection", StatusMessage: "WELCOME TO WebRTC"}
			connMsg := &MyJsonName{MyData: *MessageInfo}
			b, err := json.Marshal(connMsg)
			if err != nil {
				fmt.Println(err)
				return
			}
			welcomeMsg := []byte(b)
			chatter.send <- welcomeMsg

		case chatter := <-r.leave: //disconnection

			log.Printf("chatter leaving  with socket id %s ", chatter.socketid)
			delete(r.chatters, chatter)
			close(chatter.send)
			//db.Delete([]byte(chatter.socketid)) //remove socket
			redisData := struct {
				Token    string `redis:"token"`
				UserID   string `redis:"userid"`
				SocketId string `redis:"socketid"`
			}{}
			//find in redis if this exists if YES then return with MAP comprises UsrId/SocketId else
			exists, _ := db.GetMap(chatter.socketid, &redisData)
			if exists {
				if redisData.SocketId == chatter.socketid {
					//db.Delete(chatter.socketid)
					//deleet from active users map
					delete(activeUsers, redisData.UserID)

				}
			}

		case msgTyping := <-r.typing:
			fmt.Printf("%s", string(msgTyping))
		case msg := <-r.forward: //mesage
			//var token string
			var opType string
			data1 := FromJSONClientMsg(msg)
			//socketID := data1.Sender           //returns socket of current user
			Content := FromJSON(data1.Content) //Actual Message to be sent
			if Content != nil {
				//token := Content.Auth
				opType = Content.Type
				if opType == "newMessage" {
					fmt.Println("in NEWMESSAGE")
				}
			} else {
				//token := ""
			}

			//token := Content.Auth
			//opType := Content.Type
			switch opType {
			case "login":

				MessageInfo := &Message{StatusCode: 200, Type: "login", StatusMessage: "SUCCESS", Auth: ""}
				connMsg := &MyJsonName{MyData: *MessageInfo}
				b, err := json.Marshal(connMsg)
				if err != nil {
					fmt.Println(err)
					return
				}
				clientMsg := []byte(b)
				var chatt *Chatter
				receiverSockID := activeUsers[data1.Receiver]
				if len(receiverSockID) > 0 {
					chatt = r.GetChatter(receiverSockID)
					if chatt != nil {
						chatt.send <- clientMsg
					}
				}

			case "newMessage", "newMessageAck":
				//token no need to check here just fetch it from microDB
				getAllMsgsData := data1.Content
				getAllMsgsDataFormatted := GetAllMessagesJSON(getAllMsgsData)
				tmap := make(map[string]interface{})
				tmap["message"] = getAllMsgsDataFormatted
				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				//	receiverID := []byte(data1.Receiver)                //.Receiver)
				convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
				if len(string(convertedSocketID)) > 0 {
					chattReceiver = r.GetChatter(string(convertedSocketID))
					if chattReceiver != nil {
						fmt.Println("/******************RECEIVING SOCKET******************************/")
						fmt.Println(string(convertedSocketID))
						fmt.Println(string(recvMsg))
						chattReceiver.send <- recvMsg
					}
				}
			/*	var convertedRecvrSockID string
				senderid, _ := utils.GetSenderId(token)
				if senderid == "" {
					senderid = data1.Sender
				}
				recvrSocket, err := db.Get([]byte(data1.Receiver))
				if err != nil {
					convertedRecvrSockID = data1.Sender
				} else {
					convertedRecvrSockID = string(recvrSocket)
				}

				if senderid != "" {
					//b := []byte("Hello")
					tmap := make(map[string]interface{})
					tmap["message"] = Content
					b, err := json.Marshal(tmap)
					if err != nil {
						fmt.Println(err)
						return
					}
					recvMsg := []byte(b)
					var chatt *Chatter
					chatt = r.GetChatter(convertedRecvrSockID)
					if chatt == nil {
						chatt = r.GetChatter(senderid)
					}
					if chatt != nil {
						chatt.send <- recvMsg
					}

				}*/
			case "changeMessageStatus_1":
				changeMsgData := data1.Content
				changeMsgDataContent := FromChangeMsgStatusJSON(changeMsgData)
				//receiverUsrID := data1.Receiver //.Sender
				recvSockID, _ := activeUsers[data1.Receiver] //db.Get([]byte(receiverUsrID))
				if len(recvSockID) > 0 {
					tmap := make(map[string]interface{})
					tmap["message"] = changeMsgDataContent
					b, err := json.Marshal(tmap)
					if err != nil {
						fmt.Println(err)
						return
					}
					recvMsg := []byte(b)
					var chattReceiver *Chatter
					chattReceiver = r.GetChatter(string(recvSockID))
					formattedSocketID := string(recvSockID)
					fmt.Println()
					fmt.Printf("changeMessageStatus_1 - RECEIVER  WITH SOCKETID %s ,  USERID %s", formattedSocketID, data1.Receiver)
					if len(string(recvSockID)) > 0 {
						chattReceiver.send <- recvMsg
					}
				}
			case "changeMessageStatus_1_ack":
				changeMsgData := data1.Content
				changeMsgDataContent := FromChangeMsgStatusJSON(changeMsgData)
				tmap := make(map[string]interface{})
				tmap["message"] = changeMsgDataContent
				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				//receiverID := []byte(data1.Receiver)                //.Receiver)
				convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
				if len(convertedSocketID) > 0 {
					chattReceiver = r.GetChatter(string(convertedSocketID))
					fmt.Println()
					fmt.Printf("changeMessageStatus_1 - RECEIVER  WITH SOCKETID %s ,  USERID %s", convertedSocketID, data1.Receiver)
					//fmt.Printf("changeMessageStatus_1_ack -   RECEIVER %s WITH USERID %s", convertedSocketID, receiverID)
					if chattReceiver != nil {
						chattReceiver.send <- recvMsg
					}

				}
			//deletemessage
			case "deleteMessages":
				delMsgData := data1.Content
				delMsgDataContent := FromDeleteMsgStatusJSON(delMsgData)
				tmap := make(map[string]interface{})
				tmap["message"] = delMsgDataContent
				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				if len(data1.Receiver) > 0 {
					//	receiverID := []byte(data1.Receiver)                //.Receiver)
					convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
					if len(convertedSocketID) > 0 {
						chattReceiver = r.GetChatter(string(convertedSocketID))
						fmt.Println()
						fmt.Printf("deleteMessages -   RECEIVER %s WITH USERID %s", convertedSocketID, data1.Receiver)
						chattReceiver.send <- recvMsg
					}
				}
			//deletemessage
			case "deleteMessagesAck":
				delMsgData := data1.Content
				delMsgDataContentAck := FromDeleteMsgStatusJSON(delMsgData)
				tmap := make(map[string]interface{})
				tmap["message"] = delMsgDataContentAck
				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				if len(data1.Receiver) > 0 {
					//receiverID := []byte(data1.Receiver)                //.Receiver)
					convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
					if len(convertedSocketID) > 0 {
						chattReceiver = r.GetChatter(string(convertedSocketID))
						fmt.Println()
						fmt.Printf("deleteMessagesAck -   RECEIVER %s WITH USERID %s", convertedSocketID, data1.Receiver)
						chattReceiver.send <- recvMsg
					}
				}
			case "getServerTime":
				serverTimeMsgData := data1.Content
				serverTimeMsgDataFormatted := GetServerTimeMessagesJSON(serverTimeMsgData)
				tmap := make(map[string]interface{})
				tmap["message"] = serverTimeMsgDataFormatted
				b, err := json.Marshal(tmap)

				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				//receiverID := []byte(data1.Receiver)                //.Receiver)
				convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
				chattReceiver = r.GetChatter(string(convertedSocketID))
				fmt.Println()
				//fmt.Printf("GETSERVERTIME -   RECEIVER %s WITH USERID %s", convertedSocketID, data1.Receiver)
				if chattReceiver != nil {
					chattReceiver.send <- recvMsg
				}
			case "getAllMessagesNew":
				getAllMsgsData := data1.Content
				getAllMsgsDataFormatted := GetAllMessagesJSON(getAllMsgsData)
				tmap := make(map[string]interface{})
				tmap["message"] = getAllMsgsDataFormatted
				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				if len(data1.Receiver) > 0 {
					convertedSocketID, _ := activeUsers[data1.Receiver]
					chattReceiver = r.GetChatter(string(convertedSocketID))
					if chattReceiver != nil {
						chattReceiver.send <- recvMsg
					}
				}
			case "online", "onlineStatus":
				onlineStatusData := data1.Content
				getAllMsgsData := GetClientUserStatusJSON(onlineStatusData)
				tmap := make(map[string]interface{})
				tmap["message"] = getAllMsgsData

				b, err := json.Marshal(tmap)

				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				//receiverID := []byte(data1.Receiver)                //.Receiver)
				convertedSocketID := activeUsers[data1.Receiver] //db.Get(receiverID)

				if len(convertedSocketID) > 0 {
					fmt.Println("*******************************************************************************************************************")
					fmt.Println("USER COMMING ONLINE and STATUS IS BEING SEND TO ...." + "=" + data1.Receiver)
					fmt.Printf("ONLIEN DATA IS BEING SEND ....%s", string(b))
					fmt.Println("*******************************************************************************************************************")

					chattReceiver = r.GetChatter(string(convertedSocketID))
					if chattReceiver != nil {
						chattReceiver.send <- recvMsg
					}

				}
			case "getContactsStatus":
				getOnlineContactsData := data1.Content
				contactsData := GetAllClientUserStatusJSON(getOnlineContactsData)
				tmap := make(map[string]interface{})
				tmap["message"] = contactsData

				b, err := json.Marshal(tmap)
				if err != nil {
					fmt.Println(err)
					return
				}
				recvMsg := []byte(b)
				var chattReceiver *Chatter
				//receiverID := []byte(data1.Receiver)                //.Receiver)
				convertedSocketID, _ := activeUsers[data1.Receiver] //db.Get(receiverID)
				if len(convertedSocketID) > 0 {

					chattReceiver = r.GetChatter(string(convertedSocketID))
					chattReceiver.send <- recvMsg
				}
			default:
				fmt.Printf("DEFAULT ..........................................................")
				/*fmt.Println()
				for chatter := range r.chatters {
					select {
					case chatter.send <- msg:
					default:
						delete(r.chatters, chatter)
						close(chatter.send)
					}
				}*/
			}
			//handle calling events
		case msg := <-r.calling: //Calling events
			var CallingDataDTO CallingDataDTO
			channelMsg := FromJSONClientMsg(msg)
			callingMsg := CallingToObjectJSON(channelMsg.Content)
			//callingMsg.Type = "requestToConnectToSocket"
			//callingMsg.StatusCode = 200
			//callingMsg.StatusMessage = "success"
			//transform
			callingDataSlice, _ := json.Marshal(callingMsg.Data)
			CallingDataDTO.Data = string(callingDataSlice)
			CallingDataDTO.StatusCode = 200
			CallingDataDTO.StatusMessage = "success"
			CallingDataDTO.Type = "requestToConnectToSocket"

			tmap := make(map[string]interface{})
			tmap["message"] = CallingDataDTO //callingMsg
			b, err := json.Marshal(tmap)
			fmt.Printf("FORMATTED %s", string(b))
			if err != nil {
				fmt.Println(err)
				return
			}
			recvMsg := []byte(b)
			var chattReceiver *Chatter
			if len(channelMsg.Receiver) > 0 {
				//receiverID := []byte(channelMsg.Receiver)                //.Receiver)
				convertedSocketID, _ := activeUsers[channelMsg.Receiver] //db.Get(receiverID)
				//check if receiver exists
				fmt.Printf("Data is being send to UserId %s and SocketId %s ", channelMsg.Receiver, convertedSocketID)
				if len(convertedSocketID) > 0 {
					chattReceiver = r.GetChatter(string(convertedSocketID))
					fmt.Println()
					fmt.Printf("Calling -   RECEIVER %s WITH USERID %s", convertedSocketID, channelMsg.Receiver)
					chattReceiver.send <- recvMsg
				} else if len(convertedSocketID) == 0 && channelMsg.IsOnPushNotification {
					//else send it to push notifications
					SendForPushNotification(channelMsg.Sender, channelMsg.Receiver, string(channelMsg.Content))
				}

			}

		}

	}
}

//Get Chatter by SocketId Also will find if user is live
func (r *Room) GetChatter(socketID string) (chatter *Chatter) {
	for chatter := range r.chatters {
		if chatter.socketid == socketID {
			return chatter
		}

	}
	return nil
}

// newUUID generates a random UUID according to RFC 4122
func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
