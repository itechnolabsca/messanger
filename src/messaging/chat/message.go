package chat

import (
	"encoding/json"
	"fmt"
	"messaging/models"
	"messaging/utils"
	"messaging/viewmodels"
	"strconv"

	"github.com/pkg/errors"
)

/****************MODEL FOR NEWMESSAGE  and LOGIN***************/
type ResponseData struct {
	Message *Message `json:"message,omitempty"`
}
type ChannelMsg struct {
	Sender               string `json:"sender,omitempty"`
	Receiver             string `json:"receiver,omitempty"`
	Content              []byte `json:"message,omitempty"`
	IsOnPushNotification bool   `json:"pushnotification,omitempty"` //required to ensure if message need to be delivered on push notfications or not
}
type MyJsonName struct {
	MyData Message `json:"message,omitempty"`
}
type Message struct {
	Data          Data   `json:"data,omitempty"`
	StatusCode    int    `json:"statusCode,omitempty"`
	Type          string `json:"type,omitempty"`
	StatusMessage string `json:"statusMessage,omitempty"`
	Auth          string `json:"auth,omitempty"`
}
type Data struct {
	Server []Server `json:"server,omitempty"`
	Client Client   `json:"client,omitempty"`
	//ChangeMsgClient ChangeMessageStatusClient `json:",omitempty"`
}
type Server struct {
	Id             string        `json:"_id"`
	ClearedBy      []interface{} `json:"clearedBy"`
	EditedMessages []interface{} `json:"editedMessages"`
	LocalID        string        `json:"localId"`
	Message        string        `json:"message"`
	OperationType  string        `json:"operationType"`
	Receivers      []Receivers   `json:"receivers"` //receiver objects
	RoomID         string        `json:"roomId"`
	SendTime       int64         `json:"sendTime"`
	//SenderPhotoID  string        `json:"senderPhotoId"`
	SenderUserID string `json:"senderUserId"`
	Type         string `json:"type"`
	UpdatedAt    int64  `json:"updatedAt"`
	Thumb        string `json:"thumb"`
	MediaThumb   string `json:"mediaThumb"`
	Caption      string `json:"caption"`
}

type Client struct {
	Caption       string `json:"caption"`
	LocalID       string `json:"localId"`
	MediaThumb    string `json:"mediaThumb"`
	Message       string `json:"message"`
	OperationType string `json:"operationType"`
	RoomID        string `json:"roomId"`
	SenderPhotoID string `json:"senderPhotoId"`
	Thumb         string `json:"thumb"`
	Type          string `json:"type"`
}
type Receivers struct {
	Id        string `json:"_id"`
	Delivered bool   `json:"deliver"`
	Read      bool   `json:"read"`
	//	ReceiverPhotoID string `json:"receiverPhotoId"`
	ReceiverUserID string `json:"receiverUserId"`
	DeliveredDate  int64  `json:"deliveredDate"`
	ReadDate       int64  `json:"readDate"`
}
type ClientData struct {
	Data Client `json:"data,omitempty"`
	Type string `json:"string,omitempty"`
	Auth string `json:"string,omitempty"`
}
type CallingData struct {
	Type string `json:"type,omitempty"`
	//SenderId      string   `json:"senderId,omitempty"`
	//ReceiverId    string   `json:"receiverId,omitempty"`
	Data          CallDATA `json:"data,omitempty"`
	StatusCode    int      `json:"statusCode"`
	StatusMessage string   `json:"statusMessage"`
}
type CallingDataDTO struct {
	Type string `json:"type,omitempty"`
	//SenderId      string   `json:"senderId,omitempty"`
	//ReceiverId    string   `json:"receiverId,omitempty"`
	Data          string `json:"data,omitempty"`
	StatusCode    int    `json:"statusCode"`
	StatusMessage string `json:"statusMessage"`
}
type CallDATA struct {
	IsVideoCall  bool `json:"isVideoCall"`
	OtherDetails struct {
		_id               string `json:"_id"`
		CallID            string `json:"callId"`
		Connected         bool   `json:"connected"`
		ConnectionDetails []struct {
			_id    string `json:"_id"`
			RoomID string `json:"roomId"`
			Time   int    `json:"time"`
			UserID struct {
				AuthID          string        `json:"authId"`
				DeviceName      string        `json:"deviceName"`
				IsDeleted       bool          `json:"isDeleted"`
				MerchantAddress string        `json:"merchantAddress"`
				MerchantPhone   string        `json:"merchantPhone"`
				ProfilePics     []interface{} `json:"profilePics"`
				StoreURL        string        `json:"storeUrl"`
				UUID            string        `json:"uuid"`
			} `json:"user_id"`
		} `json:"connection_details"`
		CreatedAt     int    `json:"createdAt"`
		Favorite      bool   `json:"favorite"`
		IsDeleted     bool   `json:"isDeleted"`
		IsGroup       bool   `json:"isGroup"`
		IsMerchant    bool   `json:"isMerchant"`
		IsSlowNetwork bool   `json:"isSlowNetwork"`
		MobileDbID    string `json:"mobileDbId"`
		Name          string `json:"name"`
		OnlineStatus  bool   `json:"onlineStatus"`
		PhotoURL      string `json:"photo_url"`
		ProfilePic    bool   `json:"profilePic"`
		QrCode        string `json:"qrCode"`
		RoomID        string `json:"roomId"`
		ShowName      bool   `json:"showName"`
		SmartURL      string `json:"smartURL"`
		SortByTime    int    `json:"sortByTime"`
		SupportUser   bool   `json:"supportUser"`
		Thumb         string `json:"thumb"`
		UpdatedAt     int    `json:"updatedAt"`
	} `json:"otherDetails"`
	ReceiverID string `json:"receiverId"`
	SenderID   string `json:"senderId"`
	UserName   string `json:"userName"`
}

/**********************************************************************CONVERT CALLING JSON TO STRUCT*******************************/
func CallingToObjectJSON(jsonInput []byte) (callingData *CallingData) {
	json.Unmarshal(jsonInput, &callingData)
	return
}

/*********MODEL FOR CHANGE STATUS****************/
//REQUEST
type UpdateMessageJsonName struct {
	MyData ChangeMessageStatusRequest `json:"message,omitempty"`
}

type ChangeMessageStatusRequest struct {
	Auth          string                  `json:"auth,omitempty"`
	Data          ChangeMessageStatusData `json:"data"`
	Type          string                  `json:"type"`
	StatusCode    int                     `json:"statusCode,omitempty"`
	StatusMessage string                  `json:"statusMessage,omitempty"`
}
type ChangeMessageStatusData struct {
	Server []Server                  `json:"server"`
	Client ChangeMessageStatusClient `json:"client"`
}
type ChangeMessageStatusClient struct {
	IsSaved   string `json:"isSaved"`
	MessageID string `json:"messageId"`
	RoomID    string `json:"roomId"`
	Status    string `json:"status"`
}

func FromChangeMsgStatusJSON(jsonInput []byte) (changeMessageStatusRequest *ChangeMessageStatusRequest) {
	json.Unmarshal(jsonInput, &changeMessageStatusRequest)
	return
}
func FromChangeMsgStatusClientJSON(jsonInput []byte) (changeMessageStatusClient *ChangeMessageStatusClient) {
	json.Unmarshal(jsonInput, &changeMessageStatusClient)
	return
}

/************************END OF CHANGE MESSAGESTATUS***********/
/************************MODEL FOR DELETE MESSAGE************************************************/

//REQUEST
type DeleteMessageJsonName struct {
	MyDeleteData DeleteMessageStatusRequest `json:"message,omitempty"`
}

type DeleteMessageStatusRequest struct {
	Auth          string                  `json:"auth,omitempty"`
	Data          DeleteMessageStatusData `json:"data"`
	Type          string                  `json:"type"`
	StatusCode    int                     `json:"statusCode,omitempty"`
	StatusMessage string                  `json:"statusMessage,omitempty"`
}
type DeleteMessageStatusData struct {
	//Server []Server                  `json:"server"`

	Client DeleteMessageStatusClient `json:"client"`
}
type DeleteMessageStatusClient struct {
	RoomID     string               `json:"roomId,omitempty"`
	MessageIds []DeleteMessageIdArr `json:"messageIds,omitempty"`
	//Type string `json:"string,omitempty"`
	//Auth string `json:"string,omitempty"`
}
type DeleteMessageIdArr struct {
	ID string `json:"id,omitempty"`
}

func FromDeleteMsgStatusJSON(jsonInput []byte) (deleteMessageStatusRequest *DeleteMessageStatusRequest) {
	json.Unmarshal(jsonInput, &deleteMessageStatusRequest)
	fmt.Println(*deleteMessageStatusRequest)
	return
}
func FromDeleteMsgStatusClientJSON(jsonInput []byte) (deleteMessageStatusClient *DeleteMessageStatusClient) {
	json.Unmarshal(jsonInput, &deleteMessageStatusClient)
	return
}

/************************END OF MODEL FOR DELETE MESSAGE*******************************************/
type AllMessagesJSON struct {
	MyAllMessageJSON AllMessagesResponse `json:"message,omitempty"`
}
type AllMessagesResponse struct {
	Auth          string      `json:"auth"`
	Data          AllMessages `json:"data"`
	Type          string      `json:"type"`
	StatusCode    int         `json:"statusCode,omitempty"`
	StatusMessage string      `json:"statusMessage,omitempty"`
}

type AllMessages struct {
	Server            []Server          `json:"server"`
	ClientAllMessages ClientAllMessages `json:"client"`
}

type ClientAllMessages struct {
	DateTime int64 `json:"dateTime"`
}

func GetAllMessagesJSON(jsonInput []byte) (allMessagesResponse *AllMessagesResponse) {
	json.Unmarshal(jsonInput, &allMessagesResponse)
	return
}

/***********************GET SERVER TIME *******************************************************/
type MyServerTimeJSON struct {
	ServerTimeJSON ServerTimeJSON `json:"message,omitempty"`
}
type ServerTimeJSON struct {
	Data       MessageServerTime `json:"data,omitempty"`
	Type       string            `json:"type,omitempty"`
	StatusCode int               `json:"statusCode,omitempty"`
	Auth       string            `json:"auth,omitempty"`
}
type MessageServerTime struct {
	Client ClientMessageServerTime   `json:"client,omitempty"`
	Server []ServerMessageServerTime `json:"server,omitempty"`
}
type ClientMessageServerTime struct {
}
type ServerMessageServerTime struct {
	ServerTime int64 `json:"serverTime,omitempty"`
}

func GetServerTimeMessagesJSON(jsonInput []byte) (serverTimeJSON *ServerTimeJSON) {
	json.Unmarshal(jsonInput, &serverTimeJSON)
	return
}

/*************************END SERVER TIME *********************************************************************/
/*******************END OF GETALLMESSAGES*******************************************************************************/
/*************************************************************ONLINE/OFFLINE STATUS********************************/
type MyUserStatusJSON struct {
	UserStatusJSON UserStatusJSON `json:"message,omitempty"`
}
type UserStatusJSON struct {
	Auth          string             `json:"auth"`
	Data          DataUserStatusJSON `json:"data"`
	Type          string             `json:"type"`
	StatusCode    int                `json:"statusCode"`
	StatusMessage string             `json:"statusMessage"`
}
type DataUserStatusJSON struct {
	Server []ServerUserStatusJSON `json:"server"`
	Client ClientUserStatusJSON   `json:"client,omitempty"`
}
type ServerUserStatusJSON struct {
	UserId string `json:"userId"`
	Status bool   `json:"status"`
}

type ClientUserStatusJSON struct {
	Status bool `json:"status"`
}

//Parse json to model
func GetClientUserStatusJSON(jsonInput []byte) (myStatusJSON *UserStatusJSON) {
	json.Unmarshal(jsonInput, &myStatusJSON)
	return
}

/**************************************************************END ONLINE/OFFLINE STATUS***************************/

/**********************************************************GETALL CONTACTS STATUS********************************/
type MyAllUserStatusJSON struct {
	UserStatusJSON AllUserStatusJSON `json:"message,omitempty"`
}
type AllUserStatusJSON struct {
	Auth          string                `json:"auth"`
	Data          AllDataUserStatusJSON `json:"data"`
	Type          string                `json:"type"`
	StatusCode    int                   `json:"statusCode"`
	StatusMessage string                `json:"statusMessage"`
}
type AllDataUserStatusJSON struct {
	Server []AllServerUserStatusJSON `json:"server"`
}
type AllServerUserStatusJSON struct {
	UserId string `json:"userId"`
	Status bool   `json:"status"`
}

//Parse json to model
func GetAllClientUserStatusJSON(jsonInput []byte) (myStatusJSON *AllUserStatusJSON) {
	json.Unmarshal(jsonInput, &myStatusJSON)
	return
}

/*************************************************************END GETALLCONTACTSTATUS*************************/

func FromJSONClient(jsonInput []byte) (client *Client) {
	json.Unmarshal(jsonInput, &client)
	return
}
func FromJSONClientMsg(jsonInput []byte) (message *ChannelMsg) {
	json.Unmarshal(jsonInput, &message)
	return
}
func FromJSON(jsonInput []byte) (message *Message) {
	json.Unmarshal(jsonInput, &message)
	return
}
func validateMessage(data []byte) (Message, error) {
	var msg Message

	if err := json.Unmarshal(data, &msg); err != nil {
		return msg, errors.Wrap(err, "Unmarshaling message")
	}

	if msg.Data.Client.RoomID == "" && msg.Data.Client.Message == "" {
		return msg, errors.New("Message has no Handle or Text")
	}

	return msg, nil
}

/*******************************************************************NEW MESSAGE*******************************/
//RETURNS NUMBER OF MESSAGES SAVED
func SaveMessage(senderid string, client Client) (isSaved bool, conversation []models.Conversation) {
	var conversationArr []models.Conversation
	var savedMsgsCount = 0
	clientRoomID := client.RoomID

	receivers := GetMessageReceivers(senderid, clientRoomID)
	fmt.Printf("Receivers====> %v", receivers)
	//Below mentioned is required to send acknowledgement to sender also
	//_, convFirst := models.CreateConverstion(senderid, senderid, client.Message, client.RoomID, client.LocalID, client.OperationType, client.Type, client.Thumb, client.MediaThumb, client.Caption)
	//	conversationArr = append(conversationArr, convFirst)
	//savedMsgsCount++
	for _, rcvr := range receivers {
		isSaved, conv := models.CreateConverstion(senderid, rcvr.ReceiverUserID, client.Message, client.RoomID, client.LocalID, client.OperationType, client.Type, client.Thumb, client.MediaThumb, client.Caption)
		if isSaved == false {
			return isSaved, nil
		}
		savedMsgsCount++
		conversationArr = append(conversationArr, conv)

	}
	numOfReceivers := len(receivers) // +1
	if numOfReceivers == savedMsgsCount {
		return true, conversationArr
	}
	return false, conversationArr
}

/************GENERATE CHAT MESSAGE RESPONSE ***************************************/
func CreateMessageResponse(senderid string, client Client, convArray []models.Conversation, recvID, token, opType string) MyJsonName {

	var Data Data
	var JSONData MyJsonName
	var serverData []Server
	var Message Message
	Message.StatusCode = 200
	Message.StatusMessage = "SUCCESS"
	receivers := GetMessageConversation(convArray)
	for _, convID := range convArray {
		id := strconv.FormatInt(convID.ID, 10)
		server := Server{Id: id, RoomID: client.RoomID,
			SenderUserID: senderid, Message: client.Message, LocalID: client.LocalID, OperationType: client.OperationType,
			Type: client.Type, Thumb: client.Thumb, MediaThumb: client.MediaThumb, Caption: client.Caption,
			SendTime:  utils.MakeTimestampMilli(),
			UpdatedAt: utils.MakeTimestampMilli(), Receivers: receivers}
		serverData = append(serverData, server)
	}
	clientData := Client{Caption: client.Caption, LocalID: client.LocalID,
		MediaThumb: client.MediaThumb, Message: client.Message,
		OperationType: client.OperationType, RoomID: client.RoomID,
		SenderPhotoID: client.SenderPhotoID, Thumb: client.Thumb,
		Type: client.Type,
	}

	Data.Client = clientData
	Data.Server = serverData
	Message.Data = Data
	Message.Auth = token
	Message.Type = getMessageType(senderid, recvID, opType)
	JSONData.MyData.Auth = token
	JSONData.MyData = Message
	JSONData.MyData.Type = getMessageType(senderid, recvID, opType)
	//getMessageType(senderid, senderid)
	return JSONData

}

/*************************************************************UPDATESTATUSMESSAGE******************************/
func SaveUpdateStatusMessage(senderID string, roomID string, msgID string, delivered bool, read bool) (conversation []models.Conversation, err error) {
	return models.UpdateConversation(senderID, roomID, msgID, delivered, read)
}
func CreateUpdateMessageResponse(msgID string, senderid string, clientRec ChangeMessageStatusClient, convArray []models.Conversation, recvID, token, opType string) UpdateMessageJsonName {
	//	var filteredClient models.Conversation //this will provide orignal sender of this message
	//	var Data UpdateMessageJsonName
	var JSONData UpdateMessageJsonName
	var serverData []Server

	var Message ChangeMessageStatusRequest
	Message.StatusCode = 200
	Message.StatusMessage = "SUCCESS"
	receivers := GetMessageConversation(convArray)
	for _, convID := range convArray {
		id := strconv.FormatInt(convID.ID, 10)
		server := Server{Id: id, RoomID: convID.Roomid,
			SenderUserID: convID.From, Message: convID.Message, LocalID: convID.LocalId, OperationType: convID.OperationType,
			Type: convID.Type, Thumb: convID.Thumb, MediaThumb: convID.MediaThumb, Caption: convID.Caption,
			SendTime:  utils.MakeTimestampMilli(),
			UpdatedAt: convID.UpdatedAt, Receivers: receivers}
		serverData = append(serverData, server)
	}

	clientData := ChangeMessageStatusClient{IsSaved: clientRec.IsSaved, MessageID: clientRec.MessageID, RoomID: clientRec.RoomID, Status: clientRec.Status}
	JSONData.MyData.Data.Client = clientData //Data.Client = clientData
	//Data.ChangeMsgClient = client
	JSONData.MyData.Data.Server = serverData
	Message.Data = JSONData.MyData.Data
	//Message.Auth = token its not required in change message statuss
	Message.Type = getMessageType(senderid, recvID, opType)
	JSONData.MyData.StatusMessage = "SUCCESS"
	JSONData.MyData.Auth = token
	JSONData.MyData = Message
	//JSONData.MyData.Data = Message.Data

	JSONData.MyData.Type = getMessageType(senderid, recvID, opType)

	//fmt.Printf("%v", JSONData)
	fmt.Printf("******************************************************")

	bytes, err2 := json.Marshal(JSONData)
	if err2 != nil {
		panic(err2)
	}

	fmt.Println(string(bytes))
	return JSONData

}

func CreateUpdateMessageResponse2(msgID string, senderid string, clientRec ChangeMessageStatusClient, convArray []models.Conversation, recvID, token, opType string) UpdateMessageJsonName {
	//	var filteredClient models.Conversation //this will provide orignal sender of this message
	//	var Data UpdateMessageJsonName
	var JSONData UpdateMessageJsonName
	var serverData []Server

	var Message ChangeMessageStatusRequest
	Message.StatusCode = 200
	Message.StatusMessage = "SUCCESS"
	receivers := GetMessageConversation(convArray)
	for _, convID := range convArray {
		id := strconv.FormatInt(convID.ID, 10)
		server := Server{Id: id, RoomID: convID.Roomid,
			SenderUserID: senderid, Message: convID.Message, LocalID: convID.LocalId, OperationType: convID.OperationType,
			Type: convID.Type, Thumb: convID.Thumb, MediaThumb: convID.MediaThumb, Caption: convID.Caption,
			SendTime:  utils.MakeTimestampMilli(),
			UpdatedAt: convID.UpdatedAt, Receivers: receivers}
		serverData = append(serverData, server)
	}

	/*for _, v := range convArray {
		id := strconv.FormatInt(v.ID, 10)
		if id == msgID { //if v.From == v.To && id == msgID {
			filteredClient = v
		}

	}*/

	/*clientData := Client{Caption: filteredClient.Caption, LocalID: filteredClient.LocalId,
		MediaThumb: filteredClient.MediaThumb, Message: filteredClient.Message,
		OperationType: filteredClient.OperationType, RoomID: filteredClient.Roomid,
		SenderPhotoID: filteredClient.MediaThumb, Thumb: filteredClient.Thumb,
		Type: filteredClient.Type,
	}*/
	clientData := ChangeMessageStatusClient{IsSaved: clientRec.IsSaved, MessageID: clientRec.MessageID, RoomID: clientRec.RoomID, Status: clientRec.Status}
	JSONData.MyData.Data.Client = clientData //Data.Client = clientData
	//Data.ChangeMsgClient = client
	JSONData.MyData.Data.Server = serverData
	Message.Data = JSONData.MyData.Data
	//Message.Auth = token its not required in change message statuss
	Message.Type = getMessageType(senderid, recvID, opType)
	JSONData.MyData.StatusMessage = "SUCCESS"
	JSONData.MyData.Auth = token
	JSONData.MyData = Message
	//JSONData.MyData.Data = Message.Data

	JSONData.MyData.Type = getMessageType(senderid, recvID, opType)

	//fmt.Printf("%v", JSONData)
	fmt.Printf("******************************************************")

	bytes, err2 := json.Marshal(JSONData)
	if err2 != nil {
		panic(err2)
	}

	fmt.Println(string(bytes))
	return JSONData

}

/***********************************************RETURN SERVER TIME*****************************************/
func CreateServerTimeResponse(opType string) MyServerTimeJSON {
	var MyServerTimeJSON MyServerTimeJSON
	var ServerTimeJSON ServerTimeJSON
	var ServerMessageServerTime ServerMessageServerTime
	ServerMessageServerTime.ServerTime = utils.MakeTimestampMilli()                          //string(utils.MakeTimestampMilli())
	ServerTimeJSON.Data.Server = append(ServerTimeJSON.Data.Server, ServerMessageServerTime) //[]ServerMessageServerTime
	ServerTimeJSON.StatusCode = 200
	ServerTimeJSON.Type = opType
	MyServerTimeJSON.ServerTimeJSON = ServerTimeJSON

	jsonData, err2 := json.Marshal(MyServerTimeJSON)
	if err2 != nil {
		panic(err2)
	}

	fmt.Println(string(jsonData))

	return MyServerTimeJSON

}

/******************************************************************END OF UPDATE MESSAGE ***********************************/

/******************************************************************START OF DELETE MESSAGE *************************/

func SaveDeleteMessage(msgIDs []DeleteMessageIdArr) (IDs []DeleteMessageIdArr, recvr string) {
	var msgArr []DeleteMessageIdArr
	var msgReceiver string
	for _, msgID := range msgIDs {
		var DelMsgRecord DeleteMessageIdArr
		delConv := models.DeleteConversation(msgID.ID)

		if delConv.ID > 0 {
			msgReceiver = delConv.To
			DelMsgRecord.ID = strconv.FormatInt(delConv.ID, 10) //msgId.ID //delConv
			//msgArr = append(msgArr, delConv)
			msgArr = append(msgArr, DelMsgRecord)
		}
	}
	return msgArr, msgReceiver //models.DeleteConversation(msgIDs)

}
func CreateDeleteMessageResponse(senderID string, receiverID string, roomID string, MsgIDs []DeleteMessageIdArr, actionType string) DeleteMessageJsonName {
	var deleteMessageJsonName DeleteMessageJsonName
	var DataKey DeleteMessageStatusRequest
	var Message DeleteMessageStatusData
	var ClientData DeleteMessageStatusClient
	ClientData.RoomID = roomID
	ClientData.MessageIds = MsgIDs
	Message.Client = ClientData
	DataKey.Data = Message
	DataKey.Type = getMessageType(senderID, receiverID, actionType)
	deleteMessageJsonName.MyDeleteData = DataKey
	DataKey.StatusCode = 200
	DataKey.StatusMessage = "SUCCESS"
	deleteMessageJsonName.MyDeleteData.StatusCode = 200
	deleteMessageJsonName.MyDeleteData.StatusMessage = "SUCCESS"
	return deleteMessageJsonName

}

/****************************************************************END OF DELETE MESSAGE*****************************/
/********************************************************GETALLMESSAGES***************************************************/

func GetAllMessages(senderID string, dateTimeLimit int64) (conversation []models.Conversation, err error) {
	return models.GetAllMessagesByUserID(senderID, dateTimeLimit)
}
func GetAllMessageResponse(senderid string, client []Client, convArray []models.Conversation, recvID, token, opType string) MyJsonName {
	var filteredClient models.Conversation //this will provide orignal sender of this message
	var Data Data
	var JSONData MyJsonName
	var serverData []Server
	var Message Message
	Message.StatusCode = 200
	Message.StatusMessage = "SUCCESS"
	//receivers := GetMessageConversationById(convArray) //fetches Message receivers by MessageID supplied
	for _, convID := range convArray {
		id := strconv.FormatInt(convID.ID, 10)
		receivers := GetMessageConversationById(convID.ID)
		server := Server{Id: id, RoomID: convID.Roomid,
			SenderUserID: senderid, Message: convID.Message, LocalID: convID.LocalId, OperationType: convID.OperationType,
			Type: convID.Type, Thumb: convID.Thumb, MediaThumb: convID.MediaThumb, Caption: convID.Caption,
			SendTime:  utils.MakeTimestampMilli(),
			UpdatedAt: utils.MakeTimestampMilli(), Receivers: receivers}
		serverData = append(serverData, server)
	}

	for _, v := range convArray {
		if v.From == v.To {
			filteredClient = v
		}
	}

	clientData := Client{Caption: filteredClient.Caption, LocalID: filteredClient.LocalId,
		MediaThumb: filteredClient.MediaThumb, Message: filteredClient.Message,
		OperationType: filteredClient.OperationType, RoomID: filteredClient.Roomid,
		SenderPhotoID: filteredClient.MediaThumb, Thumb: filteredClient.Thumb,
		Type: filteredClient.Type,
	}

	Data.Client = clientData
	Data.Server = serverData

	Message.Data = Data
	//Message.Auth = token
	Message.StatusCode = 200
	Message.StatusMessage = "SUCCESS"
	Message.Type = getMessageType(senderid, recvID, opType)
	//JSONData.MyData.Auth = token
	JSONData.MyData = Message
	JSONData.MyData.Type = getMessageType(senderid, recvID, opType)
	JSONData.MyData.StatusCode = 200
	JSONData.MyData.StatusMessage = "SUCCESS"

	/*****************Print JSON**************************************************************************/
	bytes, err2 := json.Marshal(JSONData)
	if err2 != nil {
		panic(err2)
	}
	result := string(bytes)
	fmt.Println(result)
	/****************************************END JSON*******************************************************/
	return JSONData

}

/********************************************************END OF ALL MESSAGES**********************************************/
/**********************************************************USER ONLINE/OFFLINE STATUS*******************************/
func GetFriendsList(usrID string) ([]string, error) {
	var friendsList []string
	friends, err := models.GetRoomsByUserId(usrID)
	if err != nil {
		//panic(err)
		return friendsList, err
	}
	for _, friend := range friends {
		friendsList = append(friendsList, friend.Usertwo)
	}

	return friendsList, nil
}

/****************************GET FRIEND LIST FOR ONLIEN STATUS*******/
func GetFriendsListForOnline(usrID string) ([]string, error) {
	var friendsList []string
	friends, err := models.GetRoomsByUserIdForOnline(usrID)
	if err != nil {
		//panic(err)
		return friendsList, err
	}
	for _, friend := range friends {
		friendsList = append(friendsList, friend.Userone)
	}

	return friendsList, nil
}

/**************************************************CREATE FRIEND LISTS ONLINE RESPONSE AGAINST (getContactsStatus)*****/
func BuildAllContactsOnlineUserResponse(userSockets []viewmodels.Usersocket) (myUserStatusJSONData MyAllUserStatusJSON) {
	//var LiveSocketsFriends []viewmodels.Usersocket
	var MyUserStatusJSON MyAllUserStatusJSON
	//var MyDataUserStatusJSON AllDataUserStatusJSON
	var UserStatusJSON AllUserStatusJSON
	var ServerUserStatusJSONObjArr []AllServerUserStatusJSON
	for _, onlineFriend := range userSockets {
		var ServerUserStatusJSONObj AllServerUserStatusJSON
		ServerUserStatusJSONObj.Status = onlineFriend.IsLive //{status: true, userId: userID}
		ServerUserStatusJSONObj.UserId = onlineFriend.UserID
		ServerUserStatusJSONObjArr = append(ServerUserStatusJSONObjArr, ServerUserStatusJSONObj)
		UserStatusJSON.Data.Server = append(UserStatusJSON.Data.Server, ServerUserStatusJSONObj)
	}
	//	MyDataUserStatusJSON.Server = ServerUserStatusJSONObjArr
	//UserStatusJSON.Data.Server = MyDataUserStatusJSON //fmt.Printf("%v", ServerUserStatusJSONObjArr)
	//UserStatusJSON.Data.Server = ServerUserStatusJSONObjArr //MyDataUserStatusJSON

	//MyDataUserStatusJSON.Client =
	//UserStatusJSON.Data.Server = ServerUserStatusJSONObjArr
	UserStatusJSON.StatusCode = 200
	UserStatusJSON.StatusMessage = "SUCCESS"
	UserStatusJSON.Type = "getContactsStatus"
	MyUserStatusJSON.UserStatusJSON = UserStatusJSON

	fmt.Println()
	fmt.Printf("%v", MyUserStatusJSON)

	return MyUserStatusJSON
}

/********************CREATE ONLINE USER RESPONSE***************************************************/
func CreateOnlineUserResponse(friend, _type string, clientStatus bool) (myUserStatusJSONData MyUserStatusJSON) {
	var statusFlag bool
	var MyUserStatusJSON MyUserStatusJSON
	var UserStatusJSON UserStatusJSON
	var ServerUserStatusJSONObjArr []ServerUserStatusJSON

	var ClientUserStatusJSONObj ClientUserStatusJSON
	ClientUserStatusJSONObj.Status = clientStatus
	if clientStatus {
		statusFlag = true
	} else {
		statusFlag = false
	}
	if len(friend) > 0 {
		//for _, friend := range friends {
		var ServerUserStatusJSONObj ServerUserStatusJSON
		ServerUserStatusJSONObj.Status = statusFlag //{status: true, userId: userID}
		ServerUserStatusJSONObj.UserId = friend
		ServerUserStatusJSONObjArr = append(ServerUserStatusJSONObjArr, ServerUserStatusJSONObj)
	}
	//}
	//if _type != "online" {
	//	ServerUserStatusJSONObjArr = append(ServerUserStatusJSONObjArr, ServerUserStatusJSONObj)
	//}
	UserStatusJSON.Data.Server = ServerUserStatusJSONObjArr
	UserStatusJSON.Data.Client = ClientUserStatusJSONObj
	UserStatusJSON.StatusCode = 200
	UserStatusJSON.StatusMessage = "SUCCESS"
	UserStatusJSON.Type = _type
	MyUserStatusJSON.UserStatusJSON = UserStatusJSON
	return MyUserStatusJSON
}

/*
func CreateUserOnlineStatusResponse(userID string, status bool) ([]string, error) {
	var MyUserStatusJSON MyUserStatusJSON
	var UserStatusJSON UserStatusJSON
	//	var ServerUserStatusJSON []ServerUserStatusJSON
	var friendsList []string
	//fetch all friends
	friends, err := models.GetRoomsByUserId(userID)
	if err != nil {
		panic(err)
	}
	for _, friend := range friends {
		if friend.Usertwo == userID {
			UserStatusJSON.Type = "onlineStatus"

		} else {
			UserStatusJSON.Type = "online"
		}
		var ServerUserStatusJSONObjArr []ServerUserStatusJSON
		var ServerUserStatusJSONObj ServerUserStatusJSON

		ServerUserStatusJSONObj.status = true //{status: true, userId: userID}
		ServerUserStatusJSONObj.userId = userID
		ServerUserStatusJSONObjArr = append(ServerUserStatusJSONObjArr, ServerUserStatusJSONObj)
		//UserStatusJSON.Data.Server.status = true
		//UserStatusJSON.Data.Server.userId = userID
		UserStatusJSON.Data.Server = ServerUserStatusJSONObjArr
		UserStatusJSON.StatusCode = "200"
		UserStatusJSON.StatusMessage = "online status succesfull"

		//friendsList = append(friendsList, friend.Usertwo)
	}

}*/

/**********************************************************END ONLINE/OFFLINE**************************************/

/*************************************HELPER FUCNTIONS *****************/
/*********************GET MESSAGE CONVERSATION****************/
func GetMessageConversation(convArray []models.Conversation) []Receivers {
	var messageReceivers []Receivers
	for _, convID := range convArray {
		id := strconv.FormatInt(convID.ID, 10)
		recvr := Receivers{Id: id, ReceiverUserID: convID.To, ReadDate: convID.ReadAt, DeliveredDate: convID.DeliveredAt, Read: convID.Read, Delivered: convID.Delivered}
		messageReceivers = append(messageReceivers, recvr)
	}
	return messageReceivers
}

/******************************************************GET MESSAGE Conversation by MessageID*******************/
func GetMessageConversationById(convID int64) []Receivers {
	var receivers []Receivers
	var receiver Receivers
	conversation := models.GetConversationById(convID)
	receiver.Id = strconv.FormatInt(conversation.ID, 10) //convert int64 tostring
	receiver.Delivered = conversation.Delivered
	receiver.DeliveredDate = conversation.DeliveredAt
	receiver.Read = conversation.Read
	receiver.ReadDate = conversation.ReadAt
	receiver.ReceiverUserID = conversation.To
	receivers = append(receivers, receiver)
	return receivers
}

/*********************  GETMESSAGE RECEIVER HELPERS  ****************/
func GetMessageReceivers(senderID string, RoomID string) []Receivers {
	var receivers []Receivers
	receiverList, _ := models.GetFriends(senderID, RoomID)
	fmt.Printf("%v ", receiverList)
	for _, user := range receiverList {
		id := strconv.FormatUint(uint64(user.ID), 10)
		recvr := Receivers{Id: id, ReceiverUserID: user.Usertwo, ReadDate: 0, DeliveredDate: 0}
		receivers = append(receivers, recvr)
	}
	return receivers
}

/*****************HELPER FUNCTIONS******/
func getMessageType(senderid, receiverid, actionType string) string {
	switch actionType {
	case "newMessage":
		if senderid == receiverid {
			return "newMessageAck"
		}
		return "newMessage"
	case "changeMessageStatus_1":
		if senderid == receiverid {
			return "changeMessageStatus_1"
		}
		return "changeMessageStatus_1_ack"
	case "deleteMessages":
		if senderid == receiverid {
			return "deleteMessagesAck"
		}
		return "deleteMessages"
	default:
		return "getAllMessagesNew"

	}
}

/************************************************SAVE NOTIFICATION PAYLOAD TO DB*****************************/
func SendForPushNotification(sender, receiver, content string) {
	var notification models.Notification
	notification.SaveNotificationData(sender, receiver, content)
}
