package chat

import (
	"encoding/json"
	"fmt"
	"messaging/utils"

	"github.com/gorilla/websocket"
)

// Chatter represents a single Chatter
type Chatter struct {
	socket   *websocket.Conn
	send     chan []byte
	room     *Room
	socketid string //this need to change with userid
}

func (c *Chatter) read() {

	//Need to check if user is online then send it otherwise send it to push notification
	for {
		if _, msg, err := c.socket.ReadMessage(); err == nil {
			//save msg to DB
			payload := FromJSON(msg)
			opType := payload.Type
			token := payload.Auth

			if opType == "login" {
				//c.room.forward <- msg
				deviceData, _ := utils.GenerateToken(token)
				if len(deviceData.UserId) > 0 {
					senderID := deviceData.UserId
					jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: senderID, Content: msg, Receiver: senderID})
					activeUsers[deviceData.UserId] = c.socketid
					fmt.Printf("LoggedIN UserID = > %s with SOCKETID => %s", deviceData.UserId, c.socketid)

					c.room.forward <- jsonMessage
				}
			} else if opType == "newMessage" {

				var ChannelMsgArr []ChannelMsg

				senderID, _ := utils.ValidateToken(token)
				fmt.Printf("ROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOM %s", payload.Data.Client.RoomID)
				fmt.Println()
				fmt.Println("MESSSSSSSSSSSSSSSSSSSSSSSSSSSSGESENDER" + " " + senderID)
				if len(senderID) > 0 {
					isSaved, storedMessages := SaveMessage(senderID, payload.Data.Client) //saved to DB
					if isSaved {
						//create response message and receiver
						for _, message := range storedMessages {
							receiverID := message.To
							channelResponse := CreateMessageResponse(senderID, payload.Data.Client, storedMessages, receiverID, token, opType)
							messageData := channelResponse
							b, err := json.Marshal(messageData.MyData)
							if err != nil {
								fmt.Println(err)
								return
							}
							ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: senderID, Content: b, Receiver: receiverID}) // receiver.To})

						}

					}

					//send response to sender
					channelResponse := CreateMessageResponse(senderID, payload.Data.Client, storedMessages, senderID, token, opType)
					messageData := channelResponse
					b, err := json.Marshal(messageData.MyData)
					if err != nil {
						fmt.Println(err)
						return
					}
					ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: senderID, Content: b, Receiver: senderID}) // receiver.To})
					//jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: c.socketid, Content: b, Receiver: senderID})
					for _, channelMsg := range ChannelMsgArr {

						jsonMessage, _ := json.Marshal(channelMsg)
						c.room.forward <- jsonMessage

					}
				}
			} else if opType == "changeMessageStatus_1" {
				//sender will get changeMessageStatus_1_ack
				//receivers will get changeMessageStatus_1
				//var senderID string

				var updateClient ChangeMessageStatusClient
				var delivered bool
				var readCount bool
				chngMsgPayload := FromChangeMsgStatusJSON(msg)

				usrID := utils.GetUserIDBySOCKETID(token) //(chngMsgPayload.Auth)
				//dt := chngMsgPayload.Data.Client //FromChangeMsgStatusClientJSON()
				if len(usrID) > 0 {
					roomID := chngMsgPayload.Data.Client.RoomID
					msgID := chngMsgPayload.Data.Client.MessageID
					fmt.Printf("FLAGGG" + "  " + chngMsgPayload.Data.Client.Status)
					//delivered = true
					//readCount = true
					if chngMsgPayload.Data.Client.Status == "deliver" {
						delivered = true
					}
					if chngMsgPayload.Data.Client.Status == "read" {
						readCount = true
					}

					//Update Database and build response and send it on wire
					updateMessage, err := SaveUpdateStatusMessage(usrID, roomID, msgID, delivered, readCount)
					updateClient.IsSaved = chngMsgPayload.Data.Client.IsSaved //"true" //this need to change
					updateClient.MessageID = msgID
					updateClient.RoomID = roomID
					updateClient.Status = chngMsgPayload.Data.Client.Status //"read" //this need to change
					if err != nil {
						//send dummy message
						msgTyping := []byte("messageUpdation not processed..")
						c.room.typing <- msgTyping
					}
					var ChannelMsgArr []ChannelMsg
					for _, message := range updateMessage {
						//process updated message
						receiver := message
						channelResponse := CreateUpdateMessageResponse(msgID, receiver.From, updateClient, updateMessage, receiver.To, token, opType)
						messageData := channelResponse
						b, err := json.Marshal(messageData.MyData)
						if err != nil {
							fmt.Println(err)
							return
						}
						ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: usrID, Content: b, Receiver: receiver.From}) // receiver.To})
					}
					//Add sendder response
					channelResponse2 := CreateUpdateMessageResponse(msgID, usrID, updateClient, updateMessage, usrID, token, opType)
					messageData2 := channelResponse2
					a, err := json.Marshal(messageData2.MyData)
					if err != nil {
						fmt.Println(err)
						return
					}
					ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: usrID, Content: a, Receiver: usrID}) //usrID})
					for _, channelMsg := range ChannelMsgArr {
						fmt.Printf("================================================================================")
						fmt.Println()
						//fmt.Printf("updateMESSAGE  :  Sender=> %s , Receiver=> %s", channelMsg.Sender, channelMsg.Receiver)
						fmt.Println()
						jsonMessage, _ := json.Marshal(channelMsg)
						c.room.forward <- jsonMessage

					}
				}
				//send response to sender
				/*
					channelResponse2 := CreateUpdateMessageResponse(msgID, usrID, updateClient, updateMessage, usrID, token, opType)
					messageData2 := channelResponse2
					a, err := json.Marshal(messageData2.MyData)
					if err != nil {
						fmt.Println(err)
						return
					}
					jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: a, Receiver: usrID})
					c.room.forward <- jsonMessage*/

			} else if opType == "deleteMessages" {
				var ChannelMsgArr []ChannelMsg
				//var deleteClient DeleteMessageStatusClient
				delMsgPayload := FromDeleteMsgStatusJSON(msg)
				usrID := utils.GetUserIDBySOCKETID(token) //(delMsgPayload.Auth) //Payload senderid
				//	myDat := FromDeleteMsgStatusClientJSON(msg)
				//	fmt.Printf("%v", myDat)
				msgsIds := delMsgPayload.Data.Client.MessageIds
				roomID := delMsgPayload.Data.Client.RoomID
				//	fmt.Printf("MessageIds = %v , RoomId = %v", msgsIds, roomID)
				if len(msgsIds) > 0 {
					RecvIDs, delMsgRecvrID := SaveDeleteMessage(msgsIds)
					for _, recvr := range RecvIDs {
						//construct response need to check error
						channelResponse := CreateDeleteMessageResponse(usrID, recvr.ID, roomID, RecvIDs, opType)
						messageData := channelResponse
						b, err := json.Marshal(messageData.MyDeleteData)
						if err != nil {
							fmt.Println(err)
							return
						}
						/*jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: b, Receiver: recvr})
						c.room.forward <- jsonMessage
						time.Sleep(time.Second * 3) //wait for 3 seconds
						*/
						ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: usrID, Content: b, Receiver: delMsgRecvrID}) //recvr.ID}) // receiver.To})

					}
					//send to sender back
					channelResponse := CreateDeleteMessageResponse(usrID, usrID, roomID, msgsIds, opType)
					messageData := channelResponse
					b, err := json.Marshal(messageData.MyDeleteData)
					if err != nil {
						fmt.Println(err)
						return
					}
					ChannelMsgArr = append(ChannelMsgArr, ChannelMsg{Sender: usrID, Content: b, Receiver: usrID})
					//jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: b, Receiver: usrID})
					for _, channelMsg := range ChannelMsgArr {
						//fmt.Printf("================================================================================")
						//fmt.Println()
						//fmt.Printf("deleteMessage: Sender=> %s , Receiver=> %s", channelMsg.Sender, channelMsg.Receiver)
						//	fmt.Println()
						jsonMessage, _ := json.Marshal(channelMsg)
						c.room.forward <- jsonMessage

					}
					//c.room.forward <- jsonMessage

				}

			} else if opType == "getServerTime" {
				//serverTime := GetServerTimeMessagesJSON(msg)
				usrID := utils.GetUserIDBySOCKETID(token) //(serverTime.Auth)
				serverTimeJSON := CreateServerTimeResponse(opType)

				d, err := json.Marshal(serverTimeJSON.ServerTimeJSON)
				if err != nil {
					fmt.Println(err)
					return
				}
				fmt.Printf("userid= %s", usrID)
				fmt.Println()
				jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: d, Receiver: usrID})
				c.room.forward <- jsonMessage

			} else if opType == "getAllMessagesNew" { //will return all messages
				var client []Client
				allMsgPayload := GetAllMessagesJSON(msg)
				usrID := utils.GetUserIDBySOCKETID(token) //(allMsgPayload.Auth)
				fmt.Printf("userid= %s", usrID)
				fmt.Println()
				senderID := usrID //deviceData.UserDevice.UserID
				dateTimeLimit := allMsgPayload.Data.ClientAllMessages.DateTime
				allMessages, err := GetAllMessages(senderID, dateTimeLimit)
				//Send this message o Sender Only
				channelResponse := GetAllMessageResponse(senderID, client, allMessages, senderID, token, opType)
				messageData := channelResponse
				b, err := json.Marshal(messageData.MyData)
				if err != nil {
					fmt.Println(err)
					return
				}
				jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: b, Receiver: usrID})
				c.room.forward <- jsonMessage
			} else if opType == "online" {
				var ChannelMsgArr []ChannelMsg

				//track online status
				statusPayload := GetClientUserStatusJSON(msg)
				clientStatus := statusPayload.Data.Client.Status
				fmt.Printf("ONLIEN STATUS %s", clientStatus)
				usrID := utils.GetUserIDBySOCKETID(token) //(statusPayload.Auth)
				// := statusPayload.Data.Client.Status
				//build Payload
				friends, err := GetFriendsListForOnline(usrID)
				fmt.Printf("FRIEDS=================>%s", friends)
				//CreateUserOnlineStatusResponse(usrID, status)
				if err == nil {

					for _, friend := range friends {
						var ChannelMsgObj ChannelMsg
						channelResponse := CreateOnlineUserResponse(usrID, "onlineStatus", clientStatus)
						messageData := channelResponse
						b, err := json.Marshal(messageData.UserStatusJSON)
						if err != nil {
							fmt.Println(err)
							return
						}
						if friend != usrID {
							//add online friends here
							ChannelMsgObj.Sender = friend
							ChannelMsgObj.Receiver = friend
							ChannelMsgObj.Content = b
							ChannelMsgArr = append(ChannelMsgArr, ChannelMsgObj)
						}
					}
					//to sender
					var ChannelMsgObj ChannelMsg
					senderResponse := CreateOnlineUserResponse("", "online", clientStatus)
					messageData := senderResponse
					b, err := json.Marshal(messageData.UserStatusJSON)
					if err != nil {
						fmt.Println(err)
						return
					}
					ChannelMsgObj.Sender = usrID
					ChannelMsgObj.Receiver = usrID
					ChannelMsgObj.Content = b
					ChannelMsgArr = append(ChannelMsgArr, ChannelMsgObj)

					for _, channelMsg := range ChannelMsgArr {
						jsonMessage, _ := json.Marshal(&channelMsg)
						c.room.forward <- jsonMessage
					}
				}
			} else if opType == "getContactsStatus" {
				//statusPayload := GetAllClientUserStatusJSON(msg)
				//fmt.Println(statusPayload)
				usrID := utils.GetUserIDBySOCKETID(token) //(statusPayload.Auth)
				//fmt.Println("AUTH---------->" + " " + usrID)
				//fetch usrs who are friends of user
				friends, err := GetFriendsList(usrID)
				if err == nil {
					//panic(err)

					//fetch their array && build final response

					LiveUsers := utils.GetUsersLiveStatusByUserID(friends, activeUsers)
					if len(LiveUsers) > 0 {
						senderResponse := BuildAllContactsOnlineUserResponse(LiveUsers)
						messageData := senderResponse
						b, err := json.Marshal(messageData.UserStatusJSON)
						if err != nil {
							fmt.Println(err)
							return
						}
						//fmt.Println("-----------------------------------------------------------------")
						///	fmt.Printf("BEFORE %v", messageData)

						//fmt.Println("-------------------------------------------------------------------")
						//fmt.Printf(string(b))
						jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: usrID, Content: b, Receiver: usrID})
						c.room.forward <- jsonMessage

					}
				}
				//send it on channel
			} else if opType == "typing" {
				msgTyping := []byte("typing...")
				c.room.typing <- msgTyping
			} else if opType == "CallAccepted" { //|| opType == "CallRejected" || opType == "CallMissed" || opType == "EndOngoingCall" || opType == "CallControlsVideoUpdated" || opType == "CallControlsMicUpdated" || opType == "UserBusy" || opType == "OnCallHold" || opType == "OnCallResume" || opType == "CallDisconnected" || opType == "DisconnectReceived" { //calling
				callingObj := CallingToObjectJSON(msg)
				parsedData, _ := json.Marshal(callingObj)
				//	jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: callingObj.SenderId, Content: parsedData, Receiver: callingObj.ReceiverId, IsOnPushNotification: false})
				jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: callingObj.Data.SenderID, Content: parsedData, Receiver: callingObj.Data.ReceiverID, IsOnPushNotification: true})
				c.room.calling <- jsonMessage

			} else if opType == "requestToConnectToSocket" || opType == "receiverConnectedToSocket" || opType == "receiverFailedToConnectToSocket" || opType == "ContactDeleted" { //Calling and send on Push notifications
				//	fmt.Printf("%s", string(msg))
				callingObj := CallingToObjectJSON(msg)
				parsedData, _ := json.Marshal(callingObj)
				fmt.Println("########################################################################")
				//fmt.Printf("%s", string(parsedData))

				//jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: callingObj.SenderId, Content: parsedData, Receiver: callingObj.ReceiverId, IsOnPushNotification: true})
				jsonMessage, _ := json.Marshal(&ChannelMsg{Sender: callingObj.Data.SenderID, Content: parsedData, Receiver: callingObj.Data.ReceiverID, IsOnPushNotification: true})
				fmt.Println("*******************************************************")
				fmt.Printf("%s", string(jsonMessage))
				c.room.calling <- jsonMessage
			} else {
				c.room.calling <- msg
			}

		} else {
			break
		}
	}

	c.socket.Close()
}
func (c *Chatter) write() {

	for msg := range c.send {
		if err := c.socket.WriteMessage(websocket.TextMessage, msg); err != nil {
			break
		}
	}
	c.socket.Close()
}
