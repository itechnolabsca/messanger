package chat

var UserStoreDataSet map[string]string

var UStore UserStore

type UserStore struct {
	UserId   string
	SocketId string
}

func NewUserStore() (userStore UserStore) {
	UserStoreDataSet = make(map[string]string)
	return UStore
}
func (store *UserStore) Insert(userID, socketID string) (userStoreDataSet map[string]string) {
	UserStoreDataSet[userID] = socketID
	return UserStoreDataSet
}

/**********RETURNS USERID by SOCKETID************/
func (store *UserStore) Get(socketID string) string {
	if value, ok := UserStoreDataSet[socketID]; ok {
		return value
	}
	return ""
}

/***************DELETE KEY/VALUE based upon VALUE(socketId)**************/
func (store *UserStore) Delete(socketID string) {
	key := store.Get(socketID)
	delete(UserStoreDataSet, key)
}
