package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/parnurzeal/gorequest"

	"messaging/viewmodels"
)

var (
	dbPool *DBpool
)

func StoreTokens() {
	/*db := utils.DBpool.Get()
	defer db.Close()*/

	db := GetDBInstance()

	token1 := []byte("eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZCI6dHJ1ZSwic2NvcGUiOiJBIiwianRpIjoiODA2YTg2YjNiODQ2NDZjODA2MTgzZjBlZjZmYTE0OTc4N2NjZGU0YmQ4N2I3MTNiNWI2MmE4NzFmNTIzYjExZiIsImlhdCI6MTUwNjUwMzc4OCwiZXhwIjoxNTM4MDM5Nzg4fQ.lIRDOpv8N-FqmgtUrvz8GZg8Zi7q43VDUFQ_pZM__nXlbiZY3rtQt3c7PpjFN2ott42f-oDCJTRrVTH-RoewPA")
	userid1 := []byte("59cb6acf4eb69005466d496d")
	token2 := []byte("eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZCI6dHJ1ZSwic2NvcGUiOiJBIiwianRpIjoiNWNkODg2NWIxOWYyY2MyNzM5NTFmYjAzMmNlMmI3ZjU5YmI2NzNjY2EwNzM2YTY4ZWY1MDIwNmE5ZjhjYjU2OCIsImlhdCI6MTUwNjUwMzUwMywiZXhwIjoxNTM4MDM5NTAzfQ.XsmuWsCxjEJi7Qes-hitvVZlSKA_Hc3NrcP3gbGohinKZjFlRYSpEzJwxOs_a3EnBC5JWNHBTwlMOaMqqn7dJQ")
	userid2 := []byte("59cb6b704eb69005466d4979")
	db.Put(token1, userid1)
	db.Put(token2, userid2)
}

/********************************************RETURNS THE USRS WITH THEIR ONLINE FLAGS*********************/
func GetUsersLiveStatusByUserID(usrs []string, activeSockets map[string]string) []viewmodels.Usersocket {
	var UsersStatusArray []viewmodels.Usersocket
	dbPool = GetDBPool()
	db := dbPool.Get()
	defer db.Close()
	//db := GetDBInstance()
	for _, usr := range usrs {
		var serverUsertatus viewmodels.Usersocket
		serverUsertatus.UserID = usr

		isLive, err := checkUserExistsInSockets(usr, activeSockets) //db.Get([]byte(usr)) //check if DB has this user's socket id
		//fmt.Printf("SOCKET %v", string(isLive))
		if err != nil {
			serverUsertatus.IsLive = true //false
			//panic(err)
		} else {
			//If socket exists it means user/Friend  is online
			serverUsertatus.IsLive = isLive

		}
		UsersStatusArray = append(UsersStatusArray, serverUsertatus)

	}
	return UsersStatusArray
}

/******************************HELPER FUNCTION FOR CHECKING IF USER IS LIVE OR DEAD************/
func checkUserExistsInSockets(usrID string, activeSockets map[string]string) (isLive bool, err error) {
	for key := range activeSockets {
		if key == usrID {
			return true, nil
		}
		return false, nil
	}
	return false, errors.New("Unable to fetch live socket data")
}

/**********************************************VALIDATE TOKEN AGAIST REDIS******************/
func ValidateToken(token string) (usrID string, error error) {
	dbPool = GetDBPool()
	db := dbPool.Get()
	defer db.Close()
	redisData := struct {
		Name  string `redis:"name"`
		Value string `redis:"value"`
	}{}
	exists, err := db.GetMap(token, &redisData)
	if err != nil {
		return "", err
	}
	if exists {
		//fmt.Printf("%s,%s", redisData.Name, redisData.Value)
		return redisData.Value, nil
	}
	return "", errors.New("Unable to find Token in Redis DB")
}

/**********************Generate TOKEN**********************/
func GenerateToken(token string) (viewmodels.RedisPayload, error) {

	var redisPayload viewmodels.RedisPayload
	dbPool = GetDBPool()
	db := dbPool.Get()
	defer db.Close()
	//save to db

	//find in redis if this exists if YES then return with MAP comprises UsrId/SocketId else

	//db.PutMap(token, "name", token, "value", "1233")
	redisData := struct {
		Name  string `redis:"name"`
		Value string `redis:"value"`
	}{}
	exists, err := db.GetMap(token, &redisData)
	if exists {
		//need to delete pre-existing token
		//fmt.Printf("%s,%s", redisData.Name, redisData.Value)
		db.Delete(token)
		exists = false

	}

	if err != nil {
		return redisPayload, errors.New("Error retreiving tokens from REDIS...")
	} else if !exists {
		//store into redis & return that record from redis
		authData, err := invokeAuthServer(token)
		if err != nil {
			return redisPayload, errors.New("Unable to retreive token from Auth Service.....")
		}
		errs := db.PutMap(token, "name", token, "value", authData.UserDevice.UserID, "timestamp", time.Now())
		//db.PutSet(token,authData.UserDevice.UserID) //make store of tokens and related userid
		if errs != nil {
			return redisPayload, errors.New("Unable to save Redis MAP Data.. ")
		}
		redisPayload.Token = token
		redisPayload.UserId = authData.UserDevice.UserID

		return redisPayload, nil

	} else {
		//Only retreive and return
		if redisData.Name == token {
			redisPayload.Token = redisData.Name
			redisPayload.UserId = redisData.Value

		}
		return redisPayload, nil

	}
}

/***************************************************INVOKED REMOTE SERVICE FOR VALIDATION************/
func invokeAuthServer(token string) (deviceData viewmodels.UserDeviceData, err error) {
	var userDeviceData viewmodels.UserDeviceData

	//userDeviceData.UserDevice.UserID = "000007" //mocked
	//return userDeviceData, nil

	request := gorequest.New()
	resp, body, errs := request.Post("https://betacuservices.paycuapp.com/validateDeviceJwt").
		Set("authorization", "Bearer"+" "+token).
		Set("content-type", "application/json").
		Send(`{"":"` + `"}`).
		End()

	if errs != nil {
		fmt.Println(errs)
		return userDeviceData, errors.New("Unable to retreive Data rom Service AUTH..")
	}
	fmt.Println("response Body:", body)
	if resp.StatusCode == 200 {
		err := json.NewDecoder(resp.Body).Decode(&userDeviceData)
		if err != nil {
			return userDeviceData, errors.New("Unable to Parse from Response of AUTH Service..")
		}
	}

	return userDeviceData, nil

}

/***************************************GET USERIDBY SOCKETID*******************/
func GetUserIDBySOCKETID(token string) string {
	dbPool = GetDBPool()
	db := dbPool.Get()
	defer db.Close()
	//save to db
	redisData := struct {
		Name  string `redis:"name"`
		Value string `redis:"value"`
		//SocketId string `redis:"socketid"`
	}{}
	//find in redis if this exists if YES then return with MAP comprises UsrId/SocketId else
	exists, err := db.GetMap(token, &redisData)
	if err != nil {
		return ""
	}
	if exists {

		return redisData.Value
	}
	return ""
}

/********************************GET SENDERID FROM TOKENID************/
func GetSenderId(tokenString string) (key string, err error) {

	token := []byte(tokenString)
	db := GetDBInstance()
	val, err := db.Get(token)
	if err != nil {
		return "", err
	}
	return string(val), nil

}
