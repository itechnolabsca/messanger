package utils

import (
	"log"

	leveldb "github.com/syndtr/goleveldb/leveldb"
)

var leveldbConn Backend

/*****************************************************OBJECT FOR INTERACTING WITH IN-MEMORY DB************/
type Backend struct {
	db     *leveldb.DB //*db.DB
	putch  chan putRequest
	delch  chan delRequest
	quitch chan bool
}

type putRequest struct {
	key []byte
	val []byte
}

type delRequest struct {
	key []byte
}

/****************************************************RETURNS INSTANCE OF INMEMORY DB**************/
func GetDBInstance() (backend Backend) {
	return leveldbConn
}

/*****************************************************OBJECT CONSTRUCTOR************************/
func NewBackend(dbname string) (backend *Backend, err error) {
	db, err := leveldb.OpenFile(dbname, nil)

	if err != nil {
		return
	}

	log.Printf("LevelDB opened : name -> %s", dbname)

	backend = &Backend{
		db,
		make(chan putRequest),
		make(chan delRequest),
		make(chan bool),
	}
	leveldbConn = *backend
	return
}

/**************************************************INTIALIZE FIRST AND START LISTENING CHANNEL COMMUNICATION********/
func (backend *Backend) Start() {
	go func() {
		for {
			select {
			case putreq := <-backend.putch:
				backend.db.Put(putreq.key, putreq.val, nil)
				log.Printf("Backend.Put(%s, %v)\n", putreq.key, putreq.val)
			case delreq := <-backend.delch:
				backend.db.Delete(delreq.key, nil)
				log.Printf("Backend.Delete(%s)\n", delreq.key)
			case <-backend.quitch:
				close(backend.putch)
				close(backend.delch)
				close(backend.quitch)

				log.Printf("Backend stoped")
				return
			}
		}
	}()

	log.Printf("Backend started")
}

/**************************************************STOPS IN MEMORY DB*********************************/
func (backend *Backend) Shutdown() {
	backend.quitch <- true
}

/*****************************************************RETURNS VALUE BY KEY *************************/
func (backend *Backend) Get(key []byte) (val []byte, err error) {
	return backend.db.Get(key, nil)
}

/*****************************************************SAVES KEY/VALUE DATA *************************/
func (backend *Backend) Put(key, val []byte) {
	backend.putch <- putRequest{key, val}

}

/*****************************************************DELETE VALUE BY KEY *************************/
func (backend *Backend) Delete(key []byte) {
	backend.delch <- delRequest{key}
}
