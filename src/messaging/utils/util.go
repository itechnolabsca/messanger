package utils

import (
	"crypto/rand"
	"fmt"
	"io"
	"os"
	"os/exec"
	"time"
)

/***************REMOVE ELEMENT FROM INT ARRAY**************/
func RemoveIntIndex(s []int, index int) []int {
	return append(s[:index], s[index+1:]...)
}

/***************REMOVE ELEMENT FROM UINT ARRAY**************/
func RemoveUintIndex(s []uint, index uint) []uint {
	return append(s[:index], s[index+1:]...)
}
func RemoveStringIndex(s []string, index uint) []string {
	return append(s[:index], s[index+1:]...)
}

/*********************REMOVE DUPLICATES FROM ARRAY*************/
func RemoveDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

/***********CONVERT CURRENT TIME TO MILLIS*****/
func MakeTimestampMilli() int64 {
	return unixMilli(time.Now())
}
func unixMilli(t time.Time) int64 {
	return t.Round(time.Millisecond).UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
}

func ClearScreen() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}

/*************************************************** FIND USERID's COMBINATIONS BASED UPON ARRAY SUPPLIED*****/
func CombinationArray(values []string) {
	//m := map[string][]string{}
	for i := 0; i < len(values); i++ {
		for j := 0; j < len(values); j++ {
			if values[i] != values[j] {
				//fmt.Printf("%s=====> (%s,%s)", arr[i], arr[j], arr[i])

			}
		}
	}
}

/*****************************************************FUNCTION FOR CREATING NEW UNIQUE ROOM ID***********/
func GenerateRoomId() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x%x%x%x%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

/*
func PrintJSON() {
	fmt.Printf("******************************************************")

	bytes, err2 := json.Marshal(JSONData)
	if err2 != nil {
		panic(err2)
	}

	fmt.Println(string(bytes))
}*/
