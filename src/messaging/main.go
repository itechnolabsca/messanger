package main

import (
	"flag"
	"fmt"
	"html"
	"log"

	"messaging/dbmanager"
	"messaging/utils"

	"messaging/chat"

	"messaging/controllers"

	"net/http"
	"runtime"

	"github.com/gorilla/mux"
	"github.com/knadh/jsonconfig"
)

var (
	dbPool *utils.DBpool
	config *Configuration
)

func init() {
	// Load configuration from file.
	configFile := flag.String("config", "config.json", "Configuration file")

	// Command line arguments.
	flag.Parse()

	err := jsonconfig.Load(*configFile, &config)
	if err != nil {
		panic(fmt.Sprintf("Error parsing or reading the config: %v", err))
	}
	// Initialisations.
	dbPool = utils.NewDBpool(config.CacheAddress, config.CachePassword, config.CachePoolActive, config.CachePoolIdle)

}
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	uc := new(controllers.UserController)
	rc := new(controllers.RoomController)
	r := mux.NewRouter()                                 //.StrictSlash(true)
	r.HandleFunc("/user", uc.CreateUser).Methods("POST") //Required in Build
	//r.HandleFunc("/login", uc.GetUser).Methods("POST")
	//r.HandleFunc("/room/{userid}", rc.CreateRoom).Methods("POST")
	//r.HandleFunc("/room/{roomid}", rc.UpdateRoom).Methods("PUT")
	r.HandleFunc("/room", rc.CreateRoomsForUsers).Methods("POST") //Required in Build
	r.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "SOCKET SERVER IS RUNNING ...., %q", html.EscapeString(r.URL.Path))
	}).Methods("GET") //Required in Build

	http.Handle("/", r)
	//Chat Run
	chat.Run()
	/***********In MemoryDB*******************/
	/*
		dbname := flag.String("name", "chatdb.db", "Open the database with the specified name")
		flag.Parse()
		backend, err := utils.NewBackend(*dbname)
		if err != nil {
			log.Fatal("can't open database", err)
		}
		if backend != nil {
			fmt.Println("REDIS CONNECTION OPEN....")
		}

		backend.Start()
		defer backend.Shutdown()
	*/
	/********REDIS CONNECTION**************/
	db := dbPool.Get()
	if db.Conn.Err() != nil {
		panic("Redis connection failed")
	} else {
		fmt.Println("CONNECTION TO REDIS OPEN...")
	}
	db.Close()
	defer dbPool.Close()
	/**************************END REDIS*************************/
	//utils.StoreTokens() //store Initial dummy tokens
	/*********************MEMORY DB END***************/
	//database migrations
	dbmanager.RunMigrations()
	log.Printf("starting chat server on port 9090")
	http.ListenAndServe(":9090", nil)
	//log.Fatal(http.ListenAndServe(":9090", logger(r)))

}

/****************APPLICATION LOGGER***********/
func logger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}
