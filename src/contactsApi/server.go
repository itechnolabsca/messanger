package main

import (
	"contactsApi/controllers"
	"log"
	"net/http"
	"runtime"

	"fmt"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
	mgo "gopkg.in/mgo.v2"
)

const (
	WEBSERVERPORT = ":9091"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	r := httprouter.New()
	c := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}, //,
		//AllowedOrigins: []string{"http://localhost:3000/"},
	})

	h := c.Handler(r)
	pc := controllers.NewContactsController(getSession())
	r.POST("/api/v1/upload", pc.CreateContacts)
	// Print that the server is running and start the server
	fmt.Println("Server up & running on port.." + WEBSERVERPORT)
	log.Fatal(http.ListenAndServe(WEBSERVERPORT, h))

}

// getSession creates a new mongo session and panics if connection error occurs
func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")

	if err != nil {
		panic(err)
	}

	return s
}
