package models

import "gopkg.in/mgo.v2/bson"

// Contact is our struct
type Contact struct {
	ID          bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Name        string        `json:"name"`
	Phone       string        `json:"phone"`
	CountryCode string        `json:"countrycode"`
}

// Contacts is a slice of Contact
type Contacts []*Contact
