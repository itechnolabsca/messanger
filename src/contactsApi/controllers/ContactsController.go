package controllers

import (
	"contactsApi/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/**************************************************NEW CONTACT CONTROLLER POINT US TO MONGO SESSION OBJECT****************/
type ContactsController struct {
	session *mgo.Session
}

/****************************** NewPostController instatiates the controller & Session*********************************/
func NewContactsController(s *mgo.Session) *ContactsController {
	return &ContactsController{s}
}

/********************************************* CreateContacts will be upload Contacts into Database *******************/
func (pc ContactsController) CreateContacts(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//go routine
	go func() {
		c := pc.session.DB("contactdb").C("contacts")
		contacts := []models.Contact{}
		json.NewDecoder(r.Body).Decode(&contacts)
		for _, cont := range contacts {
			var contact models.Contact
			contact.ID = bson.NewObjectId()
			contact.Name = cont.Name
			contact.Phone = cont.Phone
			contact.CountryCode = cont.CountryCode
			c.Insert(contact)
		}
	}()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", "Contacts uploaded")
}
