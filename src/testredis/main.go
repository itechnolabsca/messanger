package main

import (
	"fmt"
)

type Out struct {
	Name  string `redis:"name"`
	Value int    `redis:"value"`
}

func main() {
	var dbPool *DBpool
	dbPool = NewDBpool("127.0.0.1:6379", "", 100, 80)
	// Startup db connectivity test.
	db := dbPool.Get()
	if db.conn.Err() != nil {
		panic("Redis connection failed")
	}
	//db.Close()
	//defer dbPool.Close()

	// Put a map.
	err := db.PutMap("nilmap", "name", "maptest", "value", 1)
	if err != nil {
		fmt.Printf("Putmap failed %v,", err)
	}

	// Retrieve the a map.
	out := struct {
		Name  string `redis:"name"`
		Value int    `redis:"value"`
	}{}

	exists, err := db.GetMap("nilmap", &out)
	if err != nil {
		fmt.Printf("Getmap failed %v,", err)
	}
	if exists {
		fmt.Printf("Value exists...")
		fmt.Printf("name=%s, value=%d", out.Name, out.Value)
	}
	// Key doesn't exist.
	if exists == false {
		fmt.Printf("nilmap hash key not found")
	}

	// Check values.
	if out.Name != "maptest" && out.Value != 1 {
		fmt.Printf("Map values don't match name=%s, value=%d", out.Name, out.Value)
	}
	fmt.Println("ending..........")
}
