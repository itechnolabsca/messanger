package models

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

/*********************GET DATABASE CONNECTION ******************/
func GetDBConnection() (*sql.DB, error) {
	db, err := sql.Open("mysql",
		"root:1234@tcp(127.0.0.1:3306)/chatdb")
	if err != nil {
		log.Fatal(err)
	}
	return db, err

}
