package models

type Message struct {
	ID          int64
	DeviceType  string
	DeviceToken string
	Msg         string
	/*
		Roomid        string
		From          string //From User
		To            string //To Userid
		Message       string
		CreatedAt     int64 //need to get in unix format
		Delivered     bool
		ReadCount     bool
		LocalID       string //sender's localid
		OperationType string //senders
		Type          string //sender
		Thumb         string //sender
		MediaThumb    string //sender
		Caption       string //sender*/
}
