package controllers

import (
	"errors"
	"fmt"
	"log"
	"notification/config"
	"notification/models"

	"github.com/anachronistic/apns"
)

type UserController struct {
}

/*****************RETURN INSTANCE OF NEW USER CONTROLLER*********************/
func NewUserController() *UserController {
	return new(UserController)
}

/********************** FETCH PUSH NOTIFICATION DATA  FROM DB **************/
func (uc *UserController) GetMessage() (models.Message, error) {
	db, err := models.GetDBConnection()
	if err == nil {
		defer db.Close()
		row := db.QueryRow(`SELECT id,  message , device_type,device_token FROM notifications LIMIT 1`)
		result := models.Message{}
		err = row.Scan(&result.ID, &result.Msg, &result.DeviceType, &result.DeviceToken)
		if err == nil {
			return result, nil
		} else {
			return result, errors.New("Unable to find user with device token " + result.DeviceToken)
		}

	} else {
		return models.Message{}, errors.New("Unable to get database connection")
	}

}

/**************************SEND MESSAGE TO ANDROID**************/
func (uc *UserController) SendMessageToANDROID(payload models.Message, config config.Config) bool {
	return true
}

/**************************SEND MESSAGE TO IOS**************/
func (uc *UserController) SendMessageToIOS(msgPayload models.Message, config config.Config) bool {
	var isSent bool
	payload := apns.NewPayload()
	payload.Alert = msgPayload.Msg
	payload.Badge = 42
	payload.ContentAvailable = 1
	payload.Sound = "bingbong.aiff"

	pn := apns.NewPushNotification()
	pn.DeviceToken = msgPayload.DeviceToken
	pn.AddPayload(payload)

	client := apns.NewClient(config.PushNotification.Ios.User.Gateway, config.PushNotification.Ios.User.IosApnCertificate, config.PushNotification.Ios.User.IosApnCertificate) //config.PushNotification.Ios.User.IosVoipCertificate) //("gateway.sandbox.push.apple.com:2195", "YOUR_CERT_PEM", "YOUR_KEY_NOENC_PEM")
	resp := client.Send(pn)
	alert, _ := pn.PayloadString()
	fmt.Println("  Alert:", alert)
	fmt.Println("Success:", resp.Success)
	fmt.Println("  Error:", resp.Error)
	if resp.Success == true {
		isSent = true
	} else {
		isSent = false
	}
	return isSent
}

/********************************DELETE MESSAGE FROM DB*****************/
func (uc *UserController) DeleteMessageFromDB(msgID int64) bool {
	var isDeleted bool
	db, err := models.GetDBConnection()
	if err == nil {
		defer db.Close()
		res, err := db.Exec("DELETE FROM notifications WHERE ID = ?", msgID)
		if err != nil {
			log.Fatal(err)
		}
		rowCnt, err := res.RowsAffected()
		if err != nil {
			log.Fatal(err)
		} else {
			log.Println("Deleted rows: ", rowCnt)
			isDeleted = true
		}

	}
	return isDeleted
}
