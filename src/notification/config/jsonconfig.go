package config

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	PushNotification struct {
		Android struct {
			User struct {
				BrandName string `json:"brandName"`
				FcmSender string `json:"fcmSender"`
			} `json:"user"`
		} `json:"android"`
		Ios struct {
			User struct {
				IosApnCertificate  string `json:"iosApnCertificate"`
				IosVoipCertificate string `json:"iosVoipCertificate"`
				Gateway            string `json:"gateway"`
			} `json:"user"`
		} `json:"ios"`
	} `json:"pushnotification"`
	Database struct {
		Host     string `json:"host"`
		Password string `json:"password"`
	} `json:"database"`
	Host string `json:"host"`
	Port string `json:"port"`
	Tick uint64 `json:"tick"`
}

func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}
