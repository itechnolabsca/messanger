package main

import (
	"fmt"
	"log"
	"notification/config"
	"notification/controllers"

	"github.com/jasonlvhit/gocron"
)

func main() {
	result := config.LoadConfiguration("config.json")
	s := gocron.NewScheduler()
	s.Every(result.Tick).Seconds().Do(task, result)
	if (config.Config{}) != result {
		<-s.Start()
	}

}
func task(conf config.Config) {
	uc := controllers.UserController{}
	if (controllers.UserController{}) == uc {
		//Read from database about user tokens and their msg to be sent
		payload, err := uc.GetMessage()
		if err == nil {
			if payload.DeviceType == "ANDROID" {
				isSent := uc.SendMessageToANDROID(payload, conf)
				log.Fatal("Unable to fetch message from notifications....", isSent)
			}
			if payload.DeviceType == "IOS" {
				isSent := uc.SendMessageToIOS(payload, conf)
				if isSent == true {
					fmt.Println("message sent successfully ")
					//delete Message
					uc.DeleteMessageFromDB(payload.ID)

				} else {
					fmt.Println("unable to deliver ")
				}
			}
		} else {
			fmt.Println("Unable to fetch message....")

		}
	}
}
